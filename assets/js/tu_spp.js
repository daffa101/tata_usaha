function ganti_detail_bayar(){
    var id_kelas = $("#select").val();
    
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          document.getElementById("bayar_spp").innerHTML = this.responseText;
        }
    };
    xhttp.open("POST", base_url+'index.php/Tatausaha/load_bayar_spp/'+id_kelas, true);
    xhttp.send();
}

function isi_modal_spp(nis){
  $("#metode_tabungan").html("");
  $("#data_spp").html("");
  $("#tanggal").val("");
  $('#metode').prop('selectedIndex',0);
  
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        document.getElementById("data_spp").innerHTML = this.responseText;
      }
  };
  xhttp.open("POST", base_url+'index.php/Tatausaha/load_data_spp/'+nis, true);
  xhttp.send();
}