function ganti_buat_tagihan(){
    var id_tingkatan = $("#select2").val();
    var nama = $("#buat_psb_nama").val();
    var harga = $("#buat_psb_harga").val();
    
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          $("#buat_tagihan_psb").html(this.responseText);
        }
    };
    xhttp.open("POST", base_url+'index.php/Tatausaha/buat_tagihan_psb/'+id_tingkatan+"/"+nama+"/"+harga, true);
    xhttp.send();
    
    $("#submit_form").show();
}

function update_konfirmasi_bayar(nis){
    var bayar;
    var tagihan;

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function(){
      if(this.readyState == 4 && this.status == 200){
        $("#sukses").html(this.responseText);
      }
    };
    xhttp.open("POST", base_url+'index.php/Tatausaha/update_konfirmasi/'+nis+"/"+bayar+"/"+tagihan, true);
    xhttp.send();
}

// Evic
// function ganti_info_tagihan(){
//   var id_kelas = $("#select").val();
  
//   var ajax = new XMLHttpRequest();
//   ajax.onreadystatechange = function() {
//       if (this.readyState == 4 && this.status == 200) {
//         $("#info_tagihan_siswa").html(this.responseText);
//       }
//   };
//   ajax.open("POST", base_url+'index.php/Tatausaha/load_info_tagihan_dsp/'+id_kelas, true);
//   ajax.send();
// }

// COBA Evic
$('#kelas').change(function(){
  var id_tingkat = $('#id_tingkat').val();
  var id_kelas = $('#kelas').val();
  DataTableGo('#tbl_tagihan', base_url + 'index.php/Tatausaha/load_info_tagihan_dsp/'+ id_kelas +'/'+ id_tingkat);
});

// function load tabel
function DataTableGo(element, url){
  var table_tagihan = $(element).DataTable({
    "ajax": {
      "type": "POST",
      "url": url,
      "dataSrc": function(json){
        return json;
      }
    },
    "columns":[{
      "render": function(_data, _type, _row, _meta){
        return '';
      },
      title: "No",
      "orderable": false,
      width: "5%",
      className: "text-center"
    },
    {"data": "nis", title: "NIS"},
    {"data": "nama_siswa", title: "Nama Siswa"},
    {"data": "total", title: "Total Tagihan"},
    
    {
      "render": function(_data, _type, _row, _meta){
        return '<div class="btn-group"><button id="btnDetail" class="btn btn-outline-primary">Detail Tagihan</button></div>';
      },
      title: "Action",
      "orderable": false,
      className: "text-center"
    },
  ],
  "destroy": true,
  });
  table_tagihan.on('order.dt search.dt', function(){
    table_tagihan.column(0, {search:'applied', order: 'applied'}).nodes().each(function(cell, i){
      cell.innerHTML = i + 1;
    });
  }).draw();
  return table_tagihan;
}

// Modal Detail
$('#tbl_tagihan').on('click', '#btnDetail', function(){
  var currentRow = $(this).closest("tr");
  var nis = currentRow.find("td:eq(1)").text();
  $("#modal_tagihan").modal("show");
  modal_tagihan(nis);
});

function modal_tagihan(nis){
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        $("#detail_tagihan_siswa").html(this.responseText);
      }
  };
  xhttp.open("POST", base_url+'index.php/Tatausaha/load_detail_tagihan/'+nis, true);
  xhttp.send();
}
// End Evic

function ganti_detail(){
  var id_kelas = $("#select").val();
  
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        document.getElementById("bayar_dsp").innerHTML = this.responseText;
      }
  };
  xhttp.open("POST", base_url+'index.php/Tatausaha/load_bayar_dsp/'+id_kelas, true);
  xhttp.send();
}

// Alifah
function ganti_tagihan(){
  var nis = $("#nis").val();

  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function(){
    if(this.readyState == 4 && this.status == 200){
      document.getElementById("tagihan").innerHTML = this.responseText;
    }
  };
  xhttp.open("POST", base_url+'index.php/Tatausaha/load_input_tagihan/'+nis, true);
  xhttp.send();
}
// End Alifah

function get_riwayat(){
  var nis = $("#nisRiwayat").val();
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function(){
    if(this.readyState == 4 && this.status == 200){
      document.getElementById("riwayat").innerHTML = this.responseText;
    }
  };
  xhttp.open("POST", base_url+'index.php/Tatausaha/get_riwayatBayar/'+nis, true);
  xhttp.send();
}

function modal_bayar_dsp(nis){
  //delete html sebelumnya
  $("#ajax_info").html("");
  
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        $("#_id_jenis_pembayaran").html(this.responseText);
      }
  };
  xhttp.open("POST", base_url+'index.php/Tatausaha/loadselect_bayardsp/'+nis, true);
  xhttp.send();
}

function change_jenis(){
  var id_tagihan_dsp = $("#_id_jenis_pembayaran").val();
  
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        document.getElementById("ajax_info").innerHTML = this.responseText;
      }
  };
  xhttp.open("POST", base_url+'index.php/Tatausaha/load_info_bayardsp/'+id_tagihan_dsp, true);
  xhttp.send();
}

// Daffa
$('#reservation').daterangepicker()

function ganti_tanggal(){
    var tanggal = $("#reservation").val().replace(/\//g, '-');
    
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          document.getElementById("rekap").innerHTML = this.responseText;
        }
    };
    xhttp.open("POST", base_url+'index.php/Tatausaha/load_harian/'+tanggal, true);
    xhttp.send();
}
// End Daffa