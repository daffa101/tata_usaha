function load_jenis_pembayaran(){
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          document.getElementById("jenis_pembayaran").innerHTML = this.responseText;
        }
    };
    xhttp.open("POST", base_url+'Ajax_Tu/load_jenis_pembayaran', true);
    xhttp.send();
}

load_jenis_pembayaran();

function edit_jenis_pembayaran(id_jenis_pembayaran){
    var nama = $("#nama_"+id_jenis_pembayaran).val();
    var harga = $("#harga_"+id_jenis_pembayaran).val();
    var id = id_jenis_pembayaran;
    
    $.post(base_url+"TataUsaha/edit_pembayaran_lain", 
    {
        id : id,
        nama : nama,
        harga : harga
    },
    function(){
        load_jenis_pembayaran();
        swal('Data Berhasil Diedit !' , 'Data Jenis Pembayaran Berhasil Diedit !' , 'success');
    });
}

function hapus_jenis_pembayaran(id){
    swal({
      title: "Yakin ingin menghapus ?",
      text: "Data yang dihapus tidak bisa di kembalikan !",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
        if (willDelete) {
            window.location.href = base_url+"TataUsaha/delete_pembayaran_lain/"+id;
        } 
        else {
            swal("Data Tidak Jadi Dihapus !" , "" , "error");
        }
    });
}

function ganti_detail(){
    var id_kelas = $("#select").val();
    
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          document.getElementById("deposit_tabungan").innerHTML = this.responseText;
        }
    };
    xhttp.open("POST", base_url+'Ajax_Tu/load_kelas_pembayaran_lain/'+id_kelas, true);
    xhttp.send();
}

function change_jenis(){
    var id_jenis_pembayaran = $("#id_jenis_pembayaran").val();
    
    if(id_jenis_pembayaran == 'lain_lain'){
        $("#judul").val("");
        $("#harga").val("");
    }
    else{
        //kalau selain yang "lain lain"
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
              $("#judul").val(this.responseText);
            }
        };
        xhttp.open("POST", base_url+'Ajax_Tu/load_judul_pembayaran_lain/'+id_jenis_pembayaran, true);
        xhttp.send();
        
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
              $("#harga").val(this.responseText);
            }
        };
        xhttp.open("POST", base_url+'Ajax_Tu/load_harga_pembayaran_lain/'+id_jenis_pembayaran, true);
        xhttp.send();
    }
    
}

function isi_id(nis){
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          document.getElementById("hidden_form").innerHTML = this.responseText;
        }
    };
    xhttp.open("POST", base_url+'Ajax_Tu/load_pembayaran_lain_hidden/'+nis, true);
    xhttp.send();
}

//Rekap Pembayaran
function ganti_detail_rekap(){
    var id_kelas = $("#select").val();
    
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          document.getElementById("rekap_kelas").innerHTML = this.responseText;
        }
    };
    xhttp.open("POST", base_url+'Ajax_Tu/load_rekap_pembayaran_kelas/'+id_kelas, true);
    xhttp.send();
}

function load_list_data_rekap(nis){
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          $("#rekap_detail_pembayaran").html(this.responseText);
        }
    };
    xhttp.open("POST", base_url+'Ajax_Tu/load_rekap_detail_pembayaran/'+nis, true);
    xhttp.send();
}

function ganti_tanggal(tanggal = "" , filter = ""){
    var tanggal = $("#tanggal").val();
    var filter = $("#filter").val();
    
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          $("#load_table_rekap_harian").html(this.responseText);
        }
    };
    xhttp.open("POST", base_url+'Ajax_Tu/load_detail_rekap_pembayaran_kelas/'+tanggal+'/'+filter, true);
    xhttp.send();
}