function loadDoc() {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
              document.getElementById("tbody").innerHTML = this.responseText;
            }
        };
        xhttp.open("POST", base_url+'Operator/ambil_tahun_ajaran/', true);
        xhttp.send();
}
loadDoc();

//hapus data tahun ajaran
function deleteTahun_ajaran(link){
    swal({
      title: "Yakin ingin menghapus ?",
      text: "Data yang dihapus tidak bisa di kembalikan !",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
        if (willDelete) {
            window.location.href = link;
        } 
        else {
            swal("Data Tidak Jadi Dihapus !" , "" , "error");
        }
    });
}