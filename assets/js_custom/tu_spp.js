//load kelas change
function ganti_detail(){
    var id_kelas = $("#select").val();
    
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          document.getElementById("spp").innerHTML = this.responseText;
        }
    };
    xhttp.open("POST", base_url+'Ajax_Tu/load_kelas_spp/'+id_kelas, true);
    xhttp.send();
}

//load kelas bayar
function ganti_detail_bayar(){
    var id_kelas = $("#select").val();
    
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          document.getElementById("bayar_spp").innerHTML = this.responseText;
        }
    };
    xhttp.open("POST", base_url+'Ajax_Tu/load_bayar_spp/'+id_kelas, true);
    xhttp.send();
}

//isi modal rekap
function isi_modal_spp(nis){
    $("#metode_tabungan").html("");
    $("#data_spp").html("");
    $("#tanggal").val("");
    $('#metode').prop('selectedIndex',0);
    
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          document.getElementById("data_spp").innerHTML = this.responseText;
        }
    };
    xhttp.open("POST", base_url+'Ajax_Tu/load_data_spp/'+nis, true);
    xhttp.send();
}

//isi modal rekap
function isi_modal_rekap(nis){
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          document.getElementById("load_table_rekap_spp").innerHTML = this.responseText;
        }
    };
    xhttp.open("POST", base_url+'Ajax_Tu/load_table_rekap_spp/'+nis, true);
    xhttp.send();
}

//load kelas rekap
function ganti_detail_rekap(){
    var id_kelas = $("#select").val();
    
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          document.getElementById("rekap_kelas_spp").innerHTML = this.responseText;
        }
    };
    xhttp.open("POST", base_url+'Ajax_Tu/load_kelas_rekap_spp/'+id_kelas, true);
    xhttp.send();
}


//load change spp
function change_spp(nis){
    var spp = $("#harga_spp").val();
    $.post(base_url+'TataUsaha/change_spp/'+nis,
    {
        spp : spp,
    },
    function(){
      swal('Harga SPP Diubah !' , 'Harga SPP Berhasil Diubah !' , 'success');
    });
}

//load rekap harian
function ganti_tanggal(tanggal = "" , filter = ""){
    var tanggal = $("#tanggal").val();
    var filter = $("#filter").val();
    
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          document.getElementById("load_rekap_harian_spp").innerHTML = this.responseText;
        }
    };
    xhttp.open("POST", base_url+'Ajax_Tu/load_rekap_harian_spp/'+tanggal+'/'+filter, true);
    xhttp.send();
}

function load_tanggal(tanggal){
    ganti_tanggal(tanggal);
}

function ganti_metode(){
    var metode = $("#metode").val();
    var nis = $("#nis_siswa").val();
    
    switch(metode){
        case "transfer":
            $("#foto_transfer").show();
            $("#submit_button").show();
            break;
        case "tunai":
            $("#foto_transfer").hide();
            $("#submit_button").show();
            break;
        case "tabungan":
            $("#submit_button").show();
            $("#foto_transfer").hide();
            
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                  $("#metode_tabungan").html(this.responseText);
                }
            };
            xhttp.open("POST", base_url+'Ajax_Tu/load_metode_tabungan/'+nis, true);
            xhttp.send();
            
            break;
    }
}
