//load tagihan psb
function ganti_psb(){
    var id_kelas = $("#psb_kelas").val();
    
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          document.getElementById("psb_select").innerHTML = this.responseText;
        }
    };
    xhttp.open("POST", base_url+'Ajax_Tu/load_tagihan_psb/'+id_kelas, true);
    xhttp.send();
}

//load kelas change
function ganti_info_psb(){
    var id_kelas = $("#psb_kelas").val();
    var tagihan = $("#psb_select").val();
    
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          document.getElementById("tunggakan_psb").innerHTML = this.responseText;
        }
    };
    xhttp.open("POST", base_url+'Ajax_Tu/load_kelas_tunggakan_psb/'+tagihan+'/'+id_kelas, true);
    xhttp.send();
}

//load tagihan du
function ganti_du(){
    var id_kelas = $("#du_kelas").val();
    
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          document.getElementById("du_select").innerHTML = this.responseText;
        }
    };
    xhttp.open("POST", base_url+'Ajax_Tu/load_tagihan_du/'+id_kelas, true);
    xhttp.send();
}

//load 
function ganti_info_du(){
    var id_kelas = $("#du_kelas").val();
    var tagihan = $("#du_select").val();
    
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          document.getElementById("tunggakan_du").innerHTML = this.responseText;
        }
    };
    xhttp.open("POST", base_url+'Ajax_Tu/load_kelas_tunggakan_du/'+tagihan+'/'+id_kelas, true);
    xhttp.send();
}