$("#submit_form").hide();

function hide(){
    $("#submit_button").hide();
}

function ganti_info_tagihan(){
    var id_kelas = $("#select").val();
    
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          $("#info_tagihan_psb").html(this.responseText);
        }
    };
    xhttp.open("POST", base_url+'Ajax_Tu/load_list_info_tagihan_du/'+id_kelas, true);
    xhttp.send();
}

function modal_tagihan(nis){
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          $("#detail_tagihan_psb").html(this.responseText);
        }
    };
    xhttp.open("POST", base_url+'Ajax_Tu/load_detail_tagihan_psb/'+nis, true);
    xhttp.send();
}

function modal_buat_tagihan(nis){
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          $("#form_hidden_buat_tagihan").html(this.responseText);
        }
    };
    xhttp.open("POST", base_url+'Ajax_Tu/form_hidden_buat_tagihan/'+nis, true);
    xhttp.send();
}

function ganti_buat_tagihan(){
    var id_kelas = $("#select2").val();
    var nama = $("#buat_psb_nama").val();
    var harga = $("#buat_psb_harga").val();
    
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          $("#buat_tagihan_psb").html(this.responseText);
        }
    };
    xhttp.open("POST", base_url+'Ajax_Tu/buat_tagihan_psb/'+id_kelas+"/"+nama+"/"+harga, true);
    xhttp.send();
    
    $("#submit_form").show();
}

function ganti_detail(){
    var id_kelas = $("#select").val();
    
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          document.getElementById("bayar_psb").innerHTML = this.responseText;
        }
    };
    xhttp.open("POST", base_url+'Ajax_Tu/load_bayar_psb/'+id_kelas, true);
    xhttp.send();
}

function modal_bayar_psb(nis){
    //delete html sebelumnya
    $("#ajax_info").html("");
    
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          $("#id_jenis_pembayaran").html(this.responseText);
        }
    };
    xhttp.open("POST", base_url+'Ajax_Tu/loadselect_bayarpsb/'+nis, true);
    xhttp.send();
}

function change_jenis(){
    var id_tagihan_psb = $("#id_jenis_pembayaran").val();
    
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          document.getElementById("ajax_info").innerHTML = this.responseText;
        }
    };
    xhttp.open("POST", base_url+'Ajax_Tu/load_infoselect_bayarpsb/'+id_tagihan_psb, true);
    xhttp.send();
}

//Rekap Pembayaran
function ganti_detail_rekap(){
    var id_kelas = $("#select").val();
    
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          document.getElementById("rekap_kelas").innerHTML = this.responseText;
        }
    };
    xhttp.open("POST", base_url+'Ajax_Tu/load_rekap_psb/'+id_kelas, true);
    xhttp.send();
}

function load_list_data_rekap(nis){
    
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          $("#list_data_rekap").html(this.responseText);
        }
    };
    xhttp.open("POST", base_url+'Ajax_Tu/psb_load_list_data_rekap/'+nis, true);
    xhttp.send();
}

function ganti_tanggal(tanggal = "" , filter = ""){
    var tanggal = $("#tanggal").val();
    var filter = $("#filter").val();
    
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          $("#load_table_rekap_harian").html(this.responseText);
        }
    };
    xhttp.open("POST", base_url+'Ajax_Tu/load_rekap_psb_tanggal/'+tanggal+'/'+filter, true);
    xhttp.send();
}

function ganti_metode(nis){
    var metode = $("#metode").val();
    
    switch(metode){
        case "transfer":
            $("#foto_transfer").show();
            $("#submit_button").show();
            $("#metode_tabungan").html("");
            break;
        case "tunai":
            $("#foto_transfer").hide();
            $("#submit_button").show();
            $("#metode_tabungan").html("");
            break;
        case "tabungan":
            $("#submit_button").show();
            $("#foto_transfer").hide();
            
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                  $("#metode_tabungan").html(this.responseText);
                }
            };
            xhttp.open("POST", base_url+'Ajax_Tu/load_metode_tabungan/'+nis, true);
            xhttp.send();
            
            break;
    }
}