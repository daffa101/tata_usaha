function ganti_combobox_kelas(){
    var id_tahun_ajaran = $("#tahun_ajaran").val();
    
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          document.getElementById("kelas").innerHTML = this.responseText;
        }
    };
    xhttp.open("POST", base_url+'Operator/load_combobox_kelas/'+id_tahun_ajaran, true);
    xhttp.send();
}

function load_siswa(){
    var id_kelas = $("#kelas").val();
    var tahun_prog = $("#tahun_ajaran").val();
    
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          document.getElementById("nama_siswa").innerHTML = this.responseText;
        }
    };
    xhttp.open("POST", base_url+'Operator/load_siswa/'+tahun_prog+'/'+id_kelas, true);
    xhttp.send();
}

function tampil_kelas(){
    var id_tahun_ajaran = $("#tahun_ajaran1").val();
    
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          document.getElementById("kelas_1").innerHTML = this.responseText;
        }
    };
    xhttp.open("POST", base_url+'Operator/load_combobox_kelas1/'+id_tahun_ajaran, true);
    xhttp.send();
}
