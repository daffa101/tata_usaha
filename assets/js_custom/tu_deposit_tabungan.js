function ganti_detail(){
    var id_kelas = $("#select").val();
    
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          document.getElementById("deposit_tabungan").innerHTML = this.responseText;
        }
    };
    xhttp.open("POST", base_url+'Ajax_Tu/load_kelas_deposit_tabungan/'+id_kelas, true);
    xhttp.send();
}

function isi_id(nis , tipe_tabungan){
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          document.getElementById("hidden_type").innerHTML = this.responseText;
        }
    };
    xhttp.open("POST", base_url+'Ajax_Tu/load_deposit_tabungan_hidden_type/'+nis+'/'+tipe_tabungan, true);
    xhttp.send();
}

function ganti_detail_rekap(){
    var id_kelas = $("#select").val();
    
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          document.getElementById("rekap_kelas").innerHTML = this.responseText;
        }
    };
    xhttp.open("POST", base_url+'Ajax_Tu/load_kelas_rekap_tabungan/'+id_kelas, true);
    xhttp.send();
}

function isi_modal_rekap(nis , jenis_tabungan){
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          document.getElementById("load_table_modal_rekap").innerHTML = this.responseText;
        }
    };
    xhttp.open("POST", base_url+'Ajax_Tu/load_table_modal_rekap/'+nis+'/'+jenis_tabungan, true);
    xhttp.send();
}

function ganti_tanggal(tanggal = "" , filter = ""){
    var tanggal = $("#tanggal").val();
    var filter = $("#filter").val();
    
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          document.getElementById("load_table_rekap_harian").innerHTML = this.responseText;
        }
    };
    xhttp.open("POST", base_url+'Ajax_Tu/load_table_rekap_harian/'+tanggal+'/'+filter, true);
    xhttp.send();
}

function load_tanggal(tanggal){
    ganti_tanggal(tanggal);
}

function load_data_harian(){
    var tanggal = "";
    var id_kelas = "";
    
    var tanggal = $("#tanggal").val();
    var id_kelas = $("#select").val();
    
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          $("#load_table_harian").html(this.responseText);
        }
    };
    xhttp.open("POST", base_url+'Ajax_Tu/load_detail_tabungan_harian/'+tanggal+'/'+id_kelas, true);
    xhttp.send();
}