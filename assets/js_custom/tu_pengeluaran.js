function delete_data(link){
    swal({
      title: "Yakin ingin menghapus ?",
      text: "Data yang dihapus tidak bisa di kembalikan !",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
        if (willDelete) {
            window.location.href = link;
        } 
        else {
            swal("Data Tidak Jadi Dihapus !" , "" , "error");
        }
    });
}