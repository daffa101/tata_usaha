<?php

include "connect.php";

$nis = $_POST['nis'];

$query = $con->query(
    "SELECT siswa.`nis`, siswa.`nama_siswa`, siswa.`no_telp`, siswa.`email`,
    kelas.`nama_kelas`, tagihan_siswa.`seragam_x`, tagihan_siswa.`dsp_x`, tagihan_siswa.`spp_x`,
    tagihan_siswa.`spp_xi`, tagihan_siswa.`spp_xii`,
    SUM(pembayaran.`total_pembayaran`) AS sudah_bayar
    FROM siswa
    INNER JOIN kelas ON siswa.`id_kelas` = kelas.`id_kelas`
    INNER JOIN tingkatan ON siswa.`id_tingkat` = tingkatan.`id_tingkatan`
    INNER JOIN tagihan_siswa ON siswa.`nis` = tagihan_siswa.`nis`
    LEFT JOIN pembayaran ON siswa.`nis` = pembayaran.`nis`
    WHERE siswa.`nis` = '".$nis."'");

$result = array();

while($fetch = $query->fetch_assoc()){
    $result[] = $fetch;
}

echo json_encode($result);

?>