<?php

include "connect.php";

$nis = $_REQUEST['nis'];

$query = $con->query(
    "SELECT tagihan_siswa.`dsp_x`, tagihan_siswa.`seragam_x`, tagihan_siswa.`spp_x`, tagihan_siswa.`spp_xi`, tagihan_siswa.`spp_xii`,
    SUM(pembayaran.`total_pembayaran`) AS sudah_bayar,
    (tagihan_siswa.`spp_x` + tagihan_siswa.`seragam_x` + tagihan_siswa.`dsp_x` + tagihan_siswa.`spp_xi` + tagihan_siswa.`spp_xii`) AS total_tagihan
    FROM tagihan_siswa 
    LEFT JOIN pembayaran ON tagihan_siswa.`nis` = pembayaran.`nis`
    WHERE tagihan_siswa.`nis` = '$nis'");

$result = array();

while($fetch = $query->fetch_assoc()){
    $result[] = $fetch;
}

echo json_encode($result);

?>