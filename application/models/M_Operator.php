<?php

class M_Operator extends CI_Model {
    function nav_kelas(){
        $query = $this->db->query("SELECT * from kelas");
        return $query->result();
    }

    function login($table, $where){
        return $this->db->get_where($table,$where);
    }

    function add_siswa($nis,$nama,$jk,$id_kelas,$no_telpon){
        return $this->db->query("CALL insert_siswa('$nis','$nama','$jk','$id_kelas','$no_telpon')");
    }

    function rules(){
        return [
            [
                'field' => 'nis',
                'label' => 'NIS',
                'rules' => 'required'
            ],

            [
                'field' => 'nama',
                'label' => 'Nama',
                'rules' => 'required'
            ],

            [
                'field' => 'jk',
                'label' => 'Jenis Kelamin',
                'rules' => 'required',
            ],

            [
                'field' => 'no_telp',
                'label' => 'No Telepon',
                'rules' => 'required'
            ]
        ];
    }

    function delete_siswa($where,$table){
        $this->db->where($where);
        $this->db->delete($table);
    }

    function update_data($id , $data_edit , $a , $b){
       $this->db->where($a , $id);
        if($this->db->update($b , $data_edit)){
            return true;
        }
        else{
            return false;
        }
    }

    //function buat wakel

    function insert_wakel($data){
        $this->db->insert('wali_kelas', $data);
    }

    function update_wakel($where,$data,$table){
        $this->db->where($where);
        $this->db->update($table,$data);
    }

    function delete_wk($where){
        $this->db->delete('wali_kelas',$where);
    }

    function edit_kelas($where,$data,$table){
        $this->db->where($where);
        $this->db->update($table,$data);
    }

    function return_result($a, $b = 1){
        $query = $this->db->query($a);

        if($b == 1){
            return $query->result();
        }else if($b == "row"){
            return $query->num_rows();
        }else{
            return $query->row();
        }
    }

    // Tahun Ajaran
    public function getTahunAjaran(){
        return $this->db->query("SELECT * FROM tahun_ajaran WHERE aktivasi = 'A'");
    }

    public function getAllTH(){
        return $this->db->query("SELECT * FROM tahun_ajaran");
    }

    //buat id tahun ajaran
    public function aidi(){
        return $this->db->query();
    }

    // Kelas
    public function getKelas(){
        return $this->db->query("SELECT * FROM kelas , tahun_ajaran WHERE kelas.`id_tahun_ajaran` = tahun_ajaran.`id_tahun_ajaran` AND kelas.`id_kelas` = '".$this->uri->segment(3)."'" , "baris");
    }

    public function getAllKelas(){
        return $this->db->query("SELECT * FROM kelas INNER JOIN tahun_ajaran ON kelas.`id_tahun_ajaran` = tahun_ajaran.`id_tahun_ajaran` ORDER BY id_kelas ASC");
    }

    // public function get_grade(){
    //     return $this->db->query("SELECT * FROM kelas WHERE id_tingkatan = 1");
    // }

    // Siswa
    public function getSiswa(){
        return $this->db->query("SELECT * FROM siswa WHERE id_kelas = '".$this->uri->segment(3)."' ORDER BY nama_siswa ASC");
    }

    // Wali Kelas
    public function getWakel(){
        return $this->db->query("SELECT * FROM wali_kelas INNER JOIN kelas ON wali_kelas.`id_kelas` = kelas.`id_kelas` ORDER BY nama_kelas ASC");
    }

    //function buat insert tahun anjaran
    public function insertTA($data){
        return $this->db->insert('tahun_ajaran', $data);
    }

    //buat hapus tahun ajaran
    public function delete_mta($where){
    $this->db->query("UPDATE tahun_ajaran SET aktivasi = 'B' WHERE id_tahun_ajaran = '".$where."'");
    }
}

?>