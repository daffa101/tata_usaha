<?php

class M_Tu extends CI_Model {
    public function __cosntruct(){
        parent::__cosntruct();
    }

    public function login($table , $where){
        return $this->db->get_where($table,$where);
    }

    function return_result($a, $b = 1){
        $query = $this->db->query($a);

        if($b == 1){
            return $query->result();
        }else if($b == "row"){
            return $query->num_rows();
        }else{
            return $query->row();
        }
    }

    public function getSiswa(){
        return $this->db->query("SELECT * FROM siswa");
    }

    public function nav_kelas(){
        $query = $this->db->query("SELECT * FROM kelas INNER JOIN tahun_ajaran ON kelas.`id_tahun_ajaran` = tahun_ajaran.`id_tahun_ajaran` WHERE tahun_ajaran.`aktivasi` = 'A'");
        return $query->result();
    }

    public function get_tingkatan(){
        return $this->db->query("SELECT * FROM tingkatan");
    }

    public function get_tagihan(){
        return $this->db->query("SELECT * FROM tagihan_master");
    }

    public function getTagihanSiswa($nis){
        return $this->db->query(
            "SELECT tagihan_siswa.`dsp_x`, tagihan_siswa.`seragam_x`, tagihan_siswa.`spp_x`,
             tagihan_siswa.`spp_xi`, tagihan_siswa.`spp_xii`, siswa.`nama_siswa`, siswa.`email`,
             (tagihan_siswa.`dsp_x` + tagihan_siswa.`seragam_x` + tagihan_siswa.`spp_x` + tagihan_siswa.`spp_xi` + tagihan_siswa.`spp_xii`) AS total
             FROM tagihan_siswa LEFT JOIN siswa
             ON tagihan_siswa.`nis` = siswa.`nis`       
             WHERE tagihan_siswa.`nis` = '$nis'");
    }

    public function updateKonfirmasi($id_pembayaran, $nama_petugas){
        return $this->db->query("UPDATE pembayaran SET status = 'Dikonfirmasi', nama_petugas = '$nama_petugas' WHERE id_pembayaran = '$id_pembayaran'");
    }

    public function info_tagihan_dsp($id_kelas, $id_tingkat){
        return $this->db->query(
            "SELECT *,
            tagihan_siswa.`seragam_x` + tagihan_siswa.`dsp_x` + tagihan_siswa.`spp_x` + tagihan_siswa.`spp_xi` + tagihan_siswa.`spp_xii` AS total 
            FROM siswa 
            LEFT JOIN tagihan_siswa ON siswa.`nis` = tagihan_siswa.`nis`
            WHERE id_kelas = '$id_kelas' 
            AND id_tingkat = '$id_tingkat'"
        );
    }

    //manggil tagihan buat total
    public function total_tagihan(){
        return $this->db->query("SELECT * FROM tagihan");
    }

    public function load_detail($nis){
        return $this->db->query(
            "SELECT *, 
            tagihan_siswa.`seragam_x` + tagihan_siswa.`dsp_x` + tagihan_siswa.`spp_x` + tagihan_siswa.`spp_xi` + tagihan_siswa.`spp_xii` AS total
            FROM tagihan_siswa WHERE nis = '$nis'"
        );
    }

    public function get_transfer($tanggal_awal, $tanggal_akhir){
        return $this->db->query("SELECT select_transfer('$tanggal_awal', '$tanggal_akhir') AS jml FROM pembayaran");
    }

    public function get_tunai($tanggal_awal, $tanggal_akhir){
        return $this->db->query("SELECT select_tunai('$tanggal_awal', '$tanggal_akhir') AS jml FROM pembayaran");
    }

    public function get_total_transaksi($tanggal_awal, $tanggal_akhir){
        return $this->db->query("SELECT select_total_transaksi('$tanggal_awal', '$tanggal_akhir') AS jml FROM pembayaran");
    }

    public function get_sum_all_pembayaran($tanggal_awal, $tanggal_akhir){
        return $this->db->query("SELECT select_sum_pembayaran('$tanggal_awal', '$tanggal_akhir') AS jml FROM pembayaran");
    }

    public function get_all_pembayaran($tanggal_awal, $tanggal_akhir){
        return $this->db->query("CALL get_all_pembayaran('$tanggal_awal', '$tanggal_akhir')");
    }

    public function getRiwayat($nis){
        return $this->db->query(
            "SELECT * FROM pembayaran 
            LEFT JOIN siswa ON pembayaran.`nis` = siswa.`nis` 
            WHERE pembayaran.`nis` = '$nis'
            ORDER BY timestamp DESC LIMIT 10"
        );
    }


    //untuk tambahh data tagihan
    public function insert_tagihan(){
        $post = $this->input->post();
        
            $id_tingkatan = $post["id_tingkatan"];
            $x_dsp = $post["x_dsp"];
            $x_seragam = $post["x_seragam"];
            $x_spp = $post["x_spp"];
            $xi_spp = $post["xi_spp"];
            $xii_spp = $post["xii_spp"];
            $xii_kunjin = $post["xii_kunjin"];
            $status = $post["status"];

            $this->db->query("CALL procedure_id_tagihan('$id_tingkatan','$x_dsp', '$x_spp', '$x_seragam', '$xi_spp', '$xii_spp', '$xii_kunjin', '$status');" );
        return TRUE;    
    }

    //untuk edit data tagihan
    public function edit_tagihan($where,$data,$table){
        $this->db->where($where);
        $this->db->update($table, $data);
    }

    public function getDataEditTagihan($where){
        return $this->db->query("SELECT * FROM tagihan LEFT JOIN tingkatan ON tagihan.`id_tingkatan` = tingkatan.`id_tingkatan` WHERE id_tagihan = '".$where."'");
    }

    //untuk hapus data tagihan
    public function delete_mtagihan($where){
        $this->db->query("UPDATE tagihan SET status = 'B' WHERE id_tagihan = '".$where."'");
    }

    public function getPembayaran(){
        return $this->db->query("SELECT * FROM konfirmasi_bayar");
    }

    public function getDetailPembayaran($id){
        return $this->db->query(
            "SELECT * 
             FROM pembayaran 
             LEFT JOIN siswa ON pembayaran.`nis` = siswa.`nis`
             WHERE id_pembayaran = '$id'"
        );
    }

    //add pembayaran
    public function insert_pembayaran($total_pembayaran, $metode_pembayaran, $nis, $nama_petugas){
        return $this->db->query(
        "INSERT INTO pembayaran (id_tagihan, timestamp, total_pembayaran, metode_pembayaran, nis, bukti_transfer, status, nama_petugas)
        SELECT tagihan_siswa.`id_tagihan`, NOW(), '$total_pembayaran', '$metode_pembayaran', siswa.`nis`, NULL, 'Dikonfirmasi', '$nama_petugas'
        FROM siswa
        INNER JOIN tagihan_siswa ON siswa.`nis` = tagihan_siswa.`nis`
        WHERE siswa.`nis` = '$nis'
        ");
    }

    // Get Bayar By ID
    public function getBayarById($id_pembayaran){
        return $this->db->query("SELECT * FROM pembayaran 
        LEFT JOIN tagihan_siswa ON pembayaran.`nis` = tagihan_siswa.`nis` 
        LEFT JOIN siswa ON pembayaran.`nis` = siswa.`nis`
        WHERE id_pembayaran = '$id_pembayaran'");
    }

    // Update Tagihan
    public function update_tagihanSiswa($x_seragam, $x_dsp, $x_spp, $xi_spp, $xii_spp, $total_pembayaran, $nis){
       return $this->db->query("CALL update_tagihan('$x_seragam', '$x_dsp', '$x_spp' ,'$xi_spp', '$xii_spp', '$total_pembayaran', '$nis');");
    }

    // Edit konfirmasi
    public function editKonfirmasi($where,$total){
        return $this->db->query("UPDATE pembayaran SET total_pembayaran = '$total' WHERE id_pembayaran = '$where'");
    }
    //import data excel
    public function model_import(){
        return $this->db->query("CALL import_excel");

    }

}

?>