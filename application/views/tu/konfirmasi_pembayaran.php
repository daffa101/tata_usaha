<div class="flash-data" data-flashdata="<?= $this->session->flashdata('flash'); ?>"></div>
<h2>Menu Konfirmasi Bayar</h2><hr/>

  <!--Info Tagihan-->
  <div class="tab-pane fade show active" id="custom-content-below-home" role="tabpanel" aria-labelledby="custom-content-below-home-tab" style="padding:10px;">
      <div>
          <div class="card card-success card-outline">
                <div class="card-header">

                  <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                      <i class="fas fa-minus"></i></button>
                  </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                    <table class="table table-hover table-bordered table-stripped text-center" id="simple" width="100%" cellspacing="0">
                        <thead>
                            <th style="width:8%;">No</th>
                            <th>NIS</th>
                            <th>Nama</th>
                            <th>Total Pembayaran</th>
                            <th colspan=2>Aksi</th>
                        </thead>
                        <tbody>
                        <?php 
                        $no = 1;
                        foreach($pembayaran as $p): 
                        ?>
                        <tr>
                            <td><?= $no++; ?></td>
                            <td><?= $p->nis; ?></td>
                            <td><?= $p->nama_siswa; ?></td>
                            <td><?= $p->total_pembayaran; ?></td>
                            <td><a type="button" class="btn btn-outline-primary" href="<?= base_url('index.php/Tatausaha/detail_konfirmasi/').$p->id_pembayaran; ?>">Detail</a>
                        </tr>
                        <?php endforeach; ?>
                        <?php
                        if(empty($pembayaran)){
                            echo "<tr><td colspan='6'>Tidak Ada Data !</td></tr>";
                        }
                        ?>
                        </tbody>
                    </table>
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
      </div>
  </div>

  <?php foreach($pembayaran as $p):?>
  <div class="modal fade" id="modal_edit_konfirmasi<?=$p->id_pembayaran?>" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Edit Konfirmasi</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="card-body" style="padding:0px;">
                <form role="form" method="post" action="<?= base_url("index.php/TataUsaha/edit_konfirmasi/").$p->id_pembayaran ?>" enctype="multipart/form-data">
                    <div class="card-body" style="padding:0px;">
                      <div class="form-group">
                        <label for="idp">ID Pembayaran</label>
                        <input type="text" class="form-control" name="idp" value="<?=$p->id_pembayaran?>" readonly>
                      </div>
                      <div class="form-group">
                        <label for="nis">NIS</label>
                        <input type="text" class="form-control" value="<?= $p->nis ?>" readonly>
                      </div>
                      <div class="form-group">
                        <label for="nama">Nama</label>
                        <input type="text" class="form-control" value="<?= $p->nama_siswa ?>" readonly>
                      </div>
                      <div class="form-group">
                        <label for="total">Total Pembayaran</label>
                        <input type="text" class="form-control" name="total" value="<?= $p->total_pembayaran ?>" required>
                      </div>
                    </div>
                    <div class="modal-footer text-right">
                        <button type="submit" class="btn btn-success">Edit</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
<!-- /.modal-dialog -->
</div>
  <?php endforeach; ?>
      
<script>
    const flashdata = $('.flash-data').data('flashdata');
        
    switch(flashdata){
        case "Diedit !":
            swal('Data Berhasil Di Konfirmasi !' , 'Data Telah Di Konfirmasi ! Tunggu Notifikasi Email' , 'success');
            break;
        case "edited":
            swal("Data Berhasil Di Edit !", 'Data Sudah Diedit, Silahkan Konfirmasi', 'success');
            break;
        case "Gagal !":
            swal('Data Gagal Di Konfirmasi !', 'Data Tidak Dapat Di Konfirmasi', 'error');
            break;
        case "failed":
            swal('Data Gagal Di Edit !', 'Data Tidak Dapat Di Edit', 'error');
            break;
        case "gagal email !":
			swal('Pembayaran Berhasil Masuk !' , 'Pembayaran Tetap Berhasil, Tapi Email Siswa Tidak Ditemukan' , 'warning');
			break;
    }
</script>