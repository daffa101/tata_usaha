<div class="flash-data" data flashdata=<?= $this->session->flashdata('flash'); ?>></div>
<h2>Pembayaran SPP</h2><hr>

<table class="table-sm" style="margin-bottom: 20px;">
    <tr>
        <td class="align-middle">Pilih Kelas :</td>
        <td class="align-middle">
            <select id="select" onchange="ganti_detail_bayar()" class="form-control">
                <option selected disabled>Pilih Kelas...</option>
                <?php
                    foreach($kelas as $a){?>
                        <option value="<?= $a->id_kelas ?>"><?= $a->nama_kelas ?></option>
                <?php        
                    }
                ?>
            </select>
        </td>
    </tr>
</table>

<!-- table siswanya -->
<div class="card card-success card-outline">
    <div class="card-header">
        <h3 class="card-title">Detail Siswa</h3>
    </div>
    <div class="card-body">
        <table class="table table-bordered table-stripped text-center">
            <thead>
                <th>No</th>
                <th>NIS</th>
                <th>Nama Siswa</th>
                <th>Aksi</th>
            </thead>
            <tbody id="bayar_spp">
                
            </tbody>
        </table>
    </div>
</div>

<!--MODAL ADD-->
<div class="modal fade" id="tagihan_spp" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Bayar SPP</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
            <form role="form" method="post" action="<?= base_url();?>index.php/Tatausaha/add_pembayaran_spp" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="card-body" style="padding:0px;">
                            <div class="card-body" style="padding:0px;">
                                <div id="data_spp">

                                </div>
                                <div class="form-group">
                                    <label for="tanggal">Bulan :</label>
                                    <input id="tanggal" name="tanggal" type="text" class="form-control" placeholder="Bulan - Tahun, Contoh : 09-2019">
                                </div>
                                <input type="hidden" name="metode_pembayaran" value="Tunai">
                            </div>
                        </div>
                </div>
        <div class="modal-footer text-right">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
          <button id="submit_button" type="submit" class="btn btn-success">Tambah +</button>
        </div>
        </form>
      </div>
    </div>
  </div>
<script>
    const flashdata = $('.flash-data').data('flashdata');
        
    switch(flashdata){
        case "berhasil !":
            swal('Data Berhasil Masuk !' , 'Pembayaran SPP Berhasil Diinput !' , 'success');
            break;
        case "sudah !":
            swal('Data Sudah Ada !' , 'Pembayaran SPP Sudah Dimasukkan !' , 'error');
            break;
    }
</script>
