<div class="form-group">
    <label for="judul">Total Harga</label>
    <input value="<?= "Rp. ".number_format($data->total) ?>" name="total_harga" type="text" class="form-control" placeholder="(auto) Total Harga..." disabled>
</div>
<div class="form-group">
    <label for="judul">Yang Sudah Dibayar</label>
    <input value="<?= "Rp. ".number_format($data->jml) ?>" name="sudah dibayar" type="text" class="form-control" placeholder="(auto) Sudah Dibayar..." disabled>
</div>
<div class="form-group">
    <label for="judul">Sisa Cicilan</label>
    <input value="<?= "Rp. ".number_format($data->total - $data->jml) ?>" name="sisa cicilan" type="text" class="form-control" placeholder="(auto) Sisa Cicilan" disabled>
</div>

<!--USER INPUT-->
<div class="form-group">
<label for="judul">Jumlah Cicilan yang akan Dibayar :</label>
<input id="harga" name="total" type="number" class="form-control" placeholder="Masukkan nominal" autofocus>
</div>


<div id="metode_tabungan">
</div>

<input type="hidden" name="id_tagihan_dsp" value="<?= $id_tagihan_dsp ?>">

<button id="submit_button" type="submit" class="btn btn-success">Tambah +</button>