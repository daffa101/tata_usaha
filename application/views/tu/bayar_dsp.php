<div class="flash-data" data-flashdata="<?= $this->session->flashdata('flash'); ?>"></div>
<h2>Bayar Cicilan DSP</h2><hr/>

<table class="table-sm" style="margin-bottom:20px;">
    <tr>
        <td class="align-middle">Pilih Kelas :</td>
        <td class="align-middle">
            <select onchange="ganti_detail()" id="select" class="form-control">
                <option selected disabled>Pilih Kelas...</option>
                <?php
                foreach($kelas as $a){?>
                    <option value="<?= $a->id_kelas ?>"><?= $a->nama_kelas ?></option>
                <?php
                }
                ?>
            </select>
        </td>
    </tr>
</table>

<!-- table-->
<div class="card card-success card-outline">
    <div class="card-header">
      <h3 class="card-title">Detail Siswa</h3>

      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
          <i class="fas fa-minus"></i></button>
      </div>
    </div>
    <div class="card-body">
        <table class="table table-bordered table-stripped text-center">
            <thead>
                <th>No</th>
                <th>Nama</th>
                <th>Detail</th>
            </thead>
            <tbody id="bayar_dsp">
                
            </tbody>
        </table>
    </div>
    <!-- /.card-body -->
</div>
<!-- /.card -->


<!--MODAL ADD-->
<div class="modal fade" id="modal_bayar_dsp" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Bayar Cicilan DSP</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="card-body" style="padding:0px;">
                <form role="form" method="post" action="<?= base_url("index.php/TataUsaha/add_cicilan_dsp") ?>" enctype="multipart/form-data">
                    <div class="card-body" style="padding:0px;">
                      <div class="form-group">
                        <label for="judul">Tagihan yang Dibayar</label>
                          <select id="_id_jenis_pembayaran" class="form-control" onchange="change_jenis()" required>
                              <option value="lain_lain" selected disabled>Pilih Tagihan..</option>
                          </select>
                      </div>

                      <!--SELECT AJAX & User Input-->
                      <div id="ajax_info">

                      </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="modal-footer text-right">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
<!-- /.modal-dialog -->
</div>

<script>
    const flashdata = $('.flash-data').data('flashdata');
        
    switch(flashdata){
        case "berhasil":
            swal('Data Berhasil Masuk !' , 'Pembayaran Cicilan Daftar Ulang Berhasil Diinput !' , 'success');
            break;
    }
</script>