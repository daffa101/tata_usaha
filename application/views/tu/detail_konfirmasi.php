<!-- SUMMARY-->
<div class="card card-primary card-outline">
    <div class="card-header">
      <h3 class="card-title">Detail Pembayaran atas nama <?= $detail->nama_siswa ?> </h3>
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
          <i class="fas fa-minus"></i></button>
      </div>
    </div>
    <div class="col-sm-3">
        <a style="margin-top: 10px;" href="<?= base_url("index.php/Tatausaha/konfirmasi_pembayaran")?>" class="btn btn-outline-danger">Kembali</a> <br>
    </div>
    <div class="card-body">
        <div class="row">
            <table class="table" cellpadding="5">
                <tr>
                    <td>NIS</td>
                    <td>:</td>
                    <td><?= $detail->nis ?></td>
                </tr>
                <tr>
                    <td>Nama Siswa</td>
                    <td>:</td>
                    <td><?= $detail->nama_siswa ?></td>
                </tr>
                <tr>
                    <td>Total Pembayaran</td>
                    <td>:</td>
                    <td><?= number_format($detail->total_pembayaran) ?></td>
                </tr>
                <tr>
                    <td>Waktu Pembayaran</td>
                    <td>:</td>
                    <td><?= $detail->timestamp ?></td>
                </tr>
                <tr>
                    <td>Metode Pembayaran</td>
                    <td>:</td>
                    <td><?= $detail->metode_pembayaran ?></td>
                </tr>
                <tr>
                    <td>Bukti Transfer</td>
                    <td>:</td>
                    <td><a href="<?= base_url("flutter/uploads/").$detail->bukti_transfer; ?>"><img src="<?= base_url("flutter/uploads/").$detail->bukti_transfer?>" style="width:150px; height:150px;"></a></td>
                </tr>
            </table>
            <div class="text-right">
                <a href="<?= base_url("index.php/Tatausaha/update_konfirmasi/").$detail->id_pembayaran; ?>" class="btn btn-outline-primary" id="button">Konfirmasi</a>
                <button class="btn btn-outline-warning" data-toggle="modal" data-target="#modal_edit_konfirmasi<?=$detail->id_pembayaran?>">Edit</button>
                <a href="<?= base_url("index.php/Tatausaha/hapus_konfirmasi/").$detail->id_pembayaran; ?>" class="btn btn-outline-danger" id="button">Hapus</a>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_edit_konfirmasi<?=$detail->id_pembayaran?>" aria-hidden="true">
<div class="modal-dialog modal-lg">
    <div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title">Edit Konfirmasi</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="card-body" style="padding:0px;">
            <form role="form" method="post" action="<?= base_url("index.php/TataUsaha/edit_konfirmasi/").$detail->id_pembayaran ?>" enctype="multipart/form-data">
                <div class="card-body" style="padding:0px;">
                    <div class="form-group">
                    <label for="idp">ID Pembayaran</label>
                    <input type="text" class="form-control" name="idp" value="<?=$detail->id_pembayaran?>" readonly>
                    </div>
                    <div class="form-group">
                    <label for="nis">NIS</label>
                    <input type="text" class="form-control" value="<?= $detail->nis ?>" readonly>
                    </div>
                    <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" class="form-control" value="<?= $detail->nama_siswa ?>" readonly>
                    </div>
                    <div class="form-group">
                    <label for="total">Total Pembayaran</label>
                    <input type="text" class="form-control" name="total" value="<?= $detail->total_pembayaran ?>" required>
                    </div>
                </div>
                <div class="modal-footer text-right">
                    <button type="submit" class="btn btn-success">Edit</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>
      
<script>
    const flashdata = $('.flash-data').data('flashdata');
        
    switch(flashdata){
        case "Diedit !":
            swal('Data Berhasil Di Konfirmasi !' , 'Data Telah Di Konfirmasi ! Tunggu Notifikasi Email' , 'success');
            break;
        case "edited":
            swal("Data Berhasil Di Edit !", 'Data Sudah Diedit, Silahkan Konfirmasi', 'success');
            break;
        case "Gagal !":
            swal('Data Gagal Di Konfirmasi !', 'Data Tidak Dapat Di Konfirmasi', 'error');
            break;
        case "failed":
            swal('Data Gagal Di Edit !', 'Data Tidak Dapat Di Edit', 'error');
            break;
        case "gagal email !":
			swal('Pembayaran Berhasil Masuk !' , 'Pembayaran Tetap Berhasil, Tapi Email Siswa Tidak Ditemukan' , 'warning');
			break;
    }
</script>