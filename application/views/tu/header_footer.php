<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>
        <?php
        if(isset($title)){
            echo $title;
        }
        else{
            echo "Administrasi Tata Usaha";
        }
        ?>
    </title>

    <!-- Select Option -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap3.min.css" integrity="sha256-ze/OEYGcFbPRmvCnrSeKbRTtjG4vGLHXgOqsyLFTRjg=" crossorigin="anonymous" />
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.1/css/all.css">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/adminlte.min.css">
    <!-- Css sendiri -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/style.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- SweetAlert2 -->
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <!-- daterange picker -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/plugins/daterangepicker/daterangepicker.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/plugins/datatables/dataTables.bootstrap4.css">
    
    <style>
      .form-error{
          font-size: 13px;
          font-family:Arial;
          color:red;
          font-style:italic;
          margin-bottom: 35px;
      }
    </style>
    <?php
    //activate main bar
    function active($a , $b){
        if($a == $b){
            echo "active";
        }
    }
    function menu_open($a , $b){
        if($a == $b){
            echo "menu-open";
        }
    }
    
    ?>
    <!--nyimpen global var-->
    <script>
        var base_url = "<?= base_url() ?>";
    </script>
</head>
<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <span class="brand-text font-weight-light">SMKN 11 Bandung</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="<?php echo base_url(); ?>assets/dist/img/logo_teacher.png" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">Tata Usaha</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview">
            <a href="<?php echo base_url()."index.php/TataUsaha"; ?>" class="nav-link <?php active($active , "dashboard"); ?>">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Beranda
              </p>
            </a>
          </li>
  
           <!--Pembayaran (inimah yang baru tea)-->

           <li class="nav-item has-treeview">
            <a href="<?php echo base_url()."index.php/TataUsaha/pembayaran"; ?>" class="nav-link <?php active($active , "pembayaran"); ?>" >
              <i class="nav-icon fas fa-database nav-icon"></i>
              <p>
                Pembayaran
              </p>
            </a>
            </li>

          <!--Tagihan (inimah yang baru tea)-->

            <li class="nav-item has-treeview">
            <a href="<?php echo base_url()."index.php/Tatausaha/tagihan_psb"; ?>" class="nav-link <?php active($active , "tagihan_psb"); ?>" >
              <i class="nav-icon fas fa-university nav-icon"></i>
              <p>
                Tagihan
              </p>
            </a>
            </li>


          <!--Konfirmasi (inimah yang baru tea)-->

            <li class="nav-item has-treeview">
            <a href="<?php echo base_url()."index.php/Tatausaha/konfirmasi_pembayaran"; ?>" class="nav-link <?php active($active , "konfirmasi_pembayaran"); ?>" >
              <i class="nav-icon fas fa-check-square nav-icon"></i>
              <p>
                Konfirmasi Pembayaran
              </p>
            </a>
            </li>
            
            <li class="nav-item has-treeview">
            <a href="<?php echo base_url()."index.php/Tatausaha/rekap"; ?>" class="nav-link <?php active($active , "rekap"); ?>" >
              <i class="nav-icon fas fa-book nav-icon"></i>
              <p>
                Rekap Pembayaran
              </p>
            </a>
            </li>
            
          <!--Logout-->  
          <li class="nav-item has-treeview">
            <a href="<?php echo base_url(); ?>index.php/Login/logout" class="nav-link" onclick="return confirm('Apakah Anda Yakin Akan Log Out?')">
              <i class="nav-icon fas fa-sign-out-alt"></i>
              <p>
                Keluar
              </p>
            </a>
          </li>
            
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <!-- jQuery -->
  <script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery.min.js"></script>
  <div class="content-wrapper" style="padding:15px;">
      <?php
      $this->load->view($konten);
      ?>
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2020.</strong>
    All rights reserved.
  </footer>
</div>
<!-- ./wrapper -->

    <!-- REQUIRED SCRIPTS -->
    <script>
    </script>
    <!-- jQuery -->
    <script src="<?php echo base_url();?>assets/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- overlayScrollbars -->
    <script src="<?php echo base_url(); ?>assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url(); ?>assets/dist/js/adminlte.js"></script>

    <!-- Select Option Script -->
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js" integrity="sha256-+C0A5Ilqmu4QcSPxrlGpaZxJ04VjsRjKu+G82kl5UJk=" crossorigin="anonymous"></script>
    <!-- OPTIONAL SCRIPTS -->
    <script src="<?php echo base_url(); ?>assets/dist/js/demo.js"></script>

    <!-- PAGE PLUGINS -->
    <!-- InputMask -->
    <script src="<?= base_url(); ?>assets/plugins/inputmask/jquery.inputmask.bundle.js"></script>
    <script src="<?= base_url(); ?>assets/plugins/moment/moment.min.js"></script>
    <!-- date-range-picker -->
    <script src="<?= base_url(); ?>assets/plugins/daterangepicker/daterangepicker.js"></script>
    <!-- DataTables -->
    <script src="<?= base_url(); ?>assets/plugins/datatables/jquery.dataTables.js"></script>
    <script src="<?= base_url(); ?>assets/plugins/datatables/dataTables.bootstrap4.js"></script>
    <!-- FastClick -->
    <script src="<?= base_url(); ?>assets/plugins/fastclick/fastclick.js"></script>
    <!-- JS Validasi -->
    <script src="<?= base_url(); ?>assets/js/ajax_validasi.js"></script>
    <!-- PAGE SCRIPTS -->
    <script src="<?php echo base_url(); ?>assets/dist/js/pages/dashboard2.js"></script>
    
    <!--CUSTOM JAVASCRIPT / JQUERY-->
    <script src="<?php if(isset($javascript)){echo $javascript; }else{ echo "";} ?>"></script>
    <script>
      $(function () {
        $('#simple').DataTable({
          "paging": true,
          "lengthChange": true,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false,
        });
      });
    </script>
</body>
</html>
