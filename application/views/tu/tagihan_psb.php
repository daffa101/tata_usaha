<div class="flash-data" data-flashdata="<?= $this->session->flashdata('flash'); ?>"></div>
<h2>Menu Tagihan Siswa</h2><hr/>

<ul class="nav nav-tabs" id="custom-content-below-tab" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" id="custom-content-below-home-tab" data-toggle="pill" href="#custom-content-below-home" role="tab" aria-controls="custom-content-below-home" aria-selected="true">Info Tagihan</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="custom-content-below-profile-tab" data-toggle="pill" href="#custom-content-below-profile" role="tab" aria-controls="custom-content-below-profile" aria-selected="false">Tagihan</a>
  </li>
</ul>
<div class="tab-content" id="custom-content-below-tabContent">
    
  <!--Info Tagihan-->
  <div class="tab-pane fade show active" id="custom-content-below-home" role="tabpanel" aria-labelledby="custom-content-below-home-tab" style="padding:10px;">
      <table class="table-sm" style="margin-bottom:20px;">
            <tr>
                <td class="align-middle">Pilih Kelas :</td>

                <td class="align-middle">
                    <select id="id_tingkat" class="form-control">
                        <option selected disabled>Pilih Angkatan...</option>
                        <?php
                        foreach($tingkatan as $a){?>
                            <option value="<?= $a->id_tingkatan ?>"><?= $a->tingkatan ?></option>
                        <?php
                        }
                        ?>
                    </select>
                </td>

                <td class="align-middle">
                    <!-- <select onchange="ganti_info_tagihan()" id="select" class="form-control">
                        <option selected disabled>Pilih Kelas...</option>
                        <?php
                        // foreach($kelas as $a){?>
                            <option value="<?= $a->id_kelas ?>"><?= $a->nama_kelas ?></option>
                        <?php
                        // }
                        ?>
                    </select> -->
                    <select id="kelas" class="form-control">
                        <option selected disabled>Pilih Kelas</option>
                        <?php foreach($kelas as $a){ ?>
                            <option value="<?= $a->id_kelas ?>"><?= $a->nama_kelas ?></option>
                        <?php }?> 
                    </select>
                </td>
            </tr>
       </table>
      
      <div>
          <div class="card card-success card-outline">
                <div class="card-header">
                  <h3 class="card-title">Detail Tagihan per Kelas</h3>

                  <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                      <i class="fas fa-minus"></i></button>
                  </div>
                </div>
                <div class="card-body">
                    <table id="tbl_tagihan" class="table table-hover table-bordered text-center">
                        <thead>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
      </div>
  </div>
  <!--MODAL INFO TAGIHAN-->
  <div class="modal fade" id="modal_tagihan">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Detail Tagihan DSP</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <table class="table text-center">
                <thead>
                    <th>DSP</th>
                    <th>Seragam</th>
                    <th>SPP Tahun Ke 1</th>
                    <th>SPP Tahun Ke 2</th>
                    <th>SPP Tahun Ke 3</th>
                    <th>Total</th>
                </thead>
                <tbody id="detail_tagihan_siswa">

                </tbody>
            </table>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->
      
  <!-- Buat Tagihan Massal-->
  <div class="tab-pane fade" id="custom-content-below-profile" role="tabpanel" aria-labelledby="custom-content-below-profile-tab" style="padding:10px;">

        <div id="rekap_harian">
            <div class="card card-success card-outline">
                <div class="card-header">
                  <h3 class="card-title">Tagihan</h3>

                  <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                      <i class="fas fa-minus"></i></button>
                  </div>
                </div>
                <div class="card-body">
                <div class="row">
                    <div class="col-sm-3">
                        <a  href="<?= base_url('index.php/Tatausaha/view_addtagihan'); ?>" class="btn btn-outline-primary">+ Buat Tagihan</a><br>
                        
                     </div>
                    </div><hr>
                    
                    <!-- table tagihan -->
                       <table class="table table-stripped table-hover table-bordered text-center">
                         <thead>
                            <tr>
                                <td style="width:5%">No</td>
                                <td>Angkatan</td>
                                <td>DSP</td>
                                <td>Seragam</td>
                                <td>SPP Kelas 10</td>
                                <td>SPP Kelas 11</td>
                                <td>SPP Kelas 12</td>
                                <td>Kunjin</td>
                                <td>Aksi</td>
                            </tr>
                         </thead>
                            <tbody>

                            <!-- isi table nya-->
                            <?php
                            $no = 1;
                            foreach($tagihan as $t):
                            ?>
                             <tr>
                                <td><?= $no++;?></td>
                                <td><?= $t->tingkatan; ?></td>
                                <td> Rp. <?= $t->x_dsp; ?></td>
                                <td> Rp. <?= $t->x_seragam; ?></td>
                                <td> Rp. <?= $t->x_spp; ?></td>
                                <td> Rp. <?= $t->xi_spp; ?></td>
                                <td> Rp. <?= $t->xii_spp; ?></td>
                                <td> Rp. <?= $t->xii_kunjin; ?></td>
                                <td class="text-center">
                                    <a href="<?= base_url('index.php/Tatausaha/view_edittagihan/').$t->id_tagihan; ?>" class="btn btn-outline-warning btn-sm">Edit</a>
                                    <a href="<?= base_url('index.php/Tatausaha/delete_tagihan/').$t->id_tagihan; ?>" class="btn btn-outline-danger btn-sm"> Hapus</a>
                                </td>
                             </tr>
                            <?php endforeach; ?>
                            <?php
                                if(count($tagihan) == 0):
                                    ?>
                            <tr>
                                <td colspan="6" class="text-center">Tidak Ada Data</td>
                            </tr>
                            <?php endif; ?>
                            </tbody>
                            
                        </table>
                    </form>
                </div>

    <!-- MODAL ADD BUAT TAGIHAN-->

<div class="modal fade" id="modal_add_tagihan" aria-hidden="true">
    <div class="modal-dialog modal-lg" style="width:700px;">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Buat Tagihan</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">x</span>
                </button>
            </div>
            <form action="<?= base_url("index.php/Tatausaha/add_tagihan") ?>" method="post">
            
            <div class="modal-body">
              <div class="row">
                <div class="col-md-6" id="form_modal">
                    <div class="form-group" style="width:250px;">
                        <label for="id_tagihan">ID Tagihan</label>
                        <input id="field_id_tagihan" name="id_tagihan" type="text" class="form-control" readonly>
                        <p id="validasi_field_id_tagihan" class="text-red validasi"></p>
                    </div>
                </div>

                    <div class="col-sm-6">
                    <div class="form-group" style="width:250px;">
                        <label for="tingkatan">Angkatan</label>
                        <select name="tingkatan" id="field_tingkatan" class="form-control">
                        <option value="" selected disabled>Pilih Tingkatan</option>
                        <?php
                        foreach($tingkatan as $t){ ?>
                            <option value="<?= $t->id_tingkatan; ?>"><?= $t->tingkatan; ?> </option>
                        <?php } ?>
                        </select>
                        <p class="text-red validasi" id="validasi_field_tingkatan"></p>
                    </div>
                    </div>

                    <div class="col-sm-6">
                    <div class="form-group" style="width:250px;">
                        <label for="dsp">Biaya DSP</label>
                        <input id="field_x_dsp" name="x_dsp" type="text" class="form-control" placeholder="Total Biaya DSP " >
                        <p id="validasi_field_x_dsp" class="text-red validasi"></p>
                    </div>
                    </div>

                    <div class="col-sm-6">
                    <div class="form-group" style="width:250px;">
                        <label for="seragam">Biaya Seragam</label>
                        <input id="field_x_seragam" name="x_seragam" type="text" class="form-control" placeholder="Total Biaya Seragam " >
                        <p id="validasi_field_x_seragam" class="text-red validasi"></p>
                    </div>
                    </div>

                    <div class="col-sm-6">
                    <div class="form-group" style="width:250px;">
                        <label for="spp10">Biaya SPP Kelas 10</label>
                        <input id="field_x_spp" name="x_spp" type="text" class="form-control" placeholder="Total SPP Kelas 10 " >
                        <p id="validasi_field_x_spp" class="text-red validasi"></p>
                    </div>
                    </div>

                    <div class="col-sm-6">
                    <div class="form-group" style="width:250px;">
                        <label for="spp11">Biaya SPP Kelas 11</label>
                        <input id="field_xi_spp" name="xi_spp" type="text" class="form-control" placeholder="Total SPP Kelas 11" >
                        <p id="validasi_field_xi_spp" class="text-red validasi"></p>
                    </div>
                    </div>
                    
                    <div class="col-sm-6">
                    <div class="form-group" style="width:250px;">
                        <label for="spp12">Biaya SPP Kelas 12</label>
                        <input id="field_xii_spp" name="xii_spp" type="text" class="form-control" placeholder="Total SPP Kelas 12 " >
                        <p id="validasi_field_xii_spp" class="text-red validasi"></p>
                    </div>
                    </div>

                    <div class="col-sm-6">
                    <div class="form-group" style="width:250px;">
                        <label for="kunjin">Biaya Kunjin</label>
                        <input id="field_xii_kunjin" name="xii_kunjin" type="text" class="form-control" placeholder="Total Biaya Kunjin " >
                        <p id="validasi_field_xii_kunjin" class="text-red validasi"></p>
                    </div>
                    </div>
                    <!-- div tutup yg row -->
                </div>
    
                    <input name="status" type="hidden" value="A" id="field_status" class="form-control" required>
        
                    
                    <!-- <div class="col-sm-6"> -->
                        <!-- <div id="userpass_msg">
                        <p>
                            Jika semua field sudah diisi , silahkan klik button <b>"Tambah"</b>
                        </p> -->

                        <div class="modal-footer text-right">
                        <button type="submit" class="btn btn-block btn-success">Tambah</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                        </div>

                        </div>
                    </div>
                </form>                        
                
                
            </div>
        </div>
    </div>
</div>

<?php foreach($tagihan as $t){?>

<!-- MODAL EDIT BUAT TAGIHAN-->
<div class="modal fade" id="edit_tagihan<?= $t->id_tagihan; ?>" aria-hidden="true">
    <div class="modal-dialog modal-lg" style="width:700px;">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit Data Tagihan</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">x</span>
                </button>
            </div>
            <form action="<?= base_url();?>index.php/Tatausaha/edit_tagihan/<?= $t->id_tagihan; ?>" method="post">
            <div class="modal-body">
              <div class="row">
                
                    <div class="">
                        <input id="idt" name="idt" type="hidden" value="<?= $t->id_tagihan ?>" class="form-control" placeholder="Masukkan ID" required readonly>
                    </div>

                    <div class="col-sm-6">
                    <div class="form-group" style="width:250px;">
                        <label for="edit_tingkatan">Angkatan</label>
                        <select name="edit_tingkatan" id="edit_angkatan" class="form-control">
                            <option value="<?= $t->id_tagihan; ?>" selected><?= $t->tingkatan; ?></option>
                        <?php
                        foreach($tingkatan as $ti): ?>
                            <option value="<?= $ti->tingkatan; ?>"><?= $ti->tingkatan; ?> </option>
                        <?php endforeach;  ?>
                        </select>
                    </div>
                    </div>

                    <div class="col-sm-6">
                    <div class="form-group" style="width:250px;">
                        <label for="edit_dsp">Biaya DSP :</label>
                        <input id="edit_dsp" name="edit_dsp" type="text" value="<?= $t->x_dsp ?>" class="form-control" placeholder="Contoh  :  4.000.000 " required>
                    </div>
                    </div> 

                    <div class="col-sm-6">
                    <div class="form-group" style="width:250px;">
                        <label for="edit_seragam">Biaya Seragam :</label>
                        <input id="edit_seragam" name="edit_seragam" type="text" value="<?= $t->x_seragam ?>" class="form-control" placeholder="Contoh  :  4.000.000 " required>
                    </div>
                    </div>

                    <div class="col-sm-6">
                    <div class="form-group" style="width:250px;">
                        <label for="spp10">Biaya SPP Kelas 10 :</label>
                        <input id="spp10" name="edit_spp10" type="text" value="<?= $t->x_spp ?>" class="form-control" placeholder="Contoh  :  4.000.000 " required>
                    </div>
                    </div>

                    <div class="col-sm-6">
                    <div class="form-group" style="width:250px;">
                        <label for="spp11">Biaya SPP Kelas 11 :</label>
                        <input id="spp11" name="edit_spp11" type="text" value="<?= $t->xi_spp ?>" class="form-control" placeholder="Total SPP Kelas 11" required>
                    </div>
                    </div>

                    <div class="col-sm-6">
                    <div class="form-group" style="width:250px;">
                        <label for="spp12">Biaya SPP Kelas 12 :</label>
                        <input id="spp12" name="edit_spp12" type="text" value="<?= $t->xii_spp ?>" class="form-control" placeholder="Contoh  :  4.000.000 " required>
                    </div>
                    </div>

                    <div class="col-sm-6">
                    <div class="form-group" style="width:250px;">
                        <label for="kunjin">Biaya Kunjin :</label>
                        <input id="kunjin" name="edit_kunjin" type="text" value="<?= $t->xii_kunjin ?>" class="form-control" placeholder="Contoh  :  4.000.000 " required>
                    </div>
                    </div>

                    </div>  <!-- bentar -->

                <div class="modal-footer text-right">
                 <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                 <button type="submit" class="btn btn-success">Simpan</button>
                 </div>

                </form>
             </div> <!-- tutup class row -->
            </div>
        
                <!-- /.card-body -->
            
        </div>
    </div>
</div>
<?php }?>
<script>
    const flashdata = $('.flash-data').data('flashdata');
        
    switch(flashdata){
        case "berhasil":
            swal('Data Berhasil Masuk !' , 'Tagihan Berhasil Diinput !' , 'success');
            break;
        case "Diedit !":
            swal('Data Berhasil Masuk !' , 'Tagihan Berhasil Diubah !' , 'success');
            break;
        case "Gagal":
            swal('Data Gagal Masuk !', "Data Gagal Diinput", 'error');
            break;
        case "dihapus":
            swal('Data Dihapus !', "Data Berhasil Dihapus", 'succes');
            break;
        case "gagal":
            swal('Data Gagal Dihapus !', "Data Gagal Dihapus", 'error');
            break;
    }
</script>

