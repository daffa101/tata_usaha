<div class="flash-data" data-flashdata="<?= $this->session->flashdata('flash'); ?>"></div>
<h2>Form Pembayaran Siswa</h2>
<ul class="nav nav-tabs" id="custom-content-below-tab" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" id="custom-content-below-home-tab" data-toggle="pill" href="#custom-content-below-home" role="tab" aria-controls="custom-content-below-home" aria-selected="true">Pembayaran</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="custom-content-below-profile-tab" data-toggle="pill" href="#custom-content-below-profile" role="tab" aria-controls="custom-content-below-profile" aria-selected="false">Riwayat Pembayaran</a>
  </li>
</ul>
<div class="tab-content" id="custom-content-below-tabContent">

<hr>
<div class="tab-pane fade show active" id="custom-content-below-home" role="tabpanel" aria-labelledby="custom-content-below-home-tab" style="padding:10px;">
	<form action="<?= base_url('index.php/Tatausaha/validasi_bayar')?>" method="post">
		<div class="card-body" style="padding:0px;">
			<div class="form-group">
				<label class="label">NIS</label>
				<select class="form-control" name="nis" id="nis" onchange="ganti_tagihan()" placeholder="Pilih NIS atau Nama Siswa">
					<option value="" selected>-- Pilih Siswa --</option>
					<?php
					foreach($siswa as $s){?>
					<option value="<?= $s->nis; ?>"><?= $s->nis; ?> - <?= $s->nama_siswa; ?></option>
					<?php
						}
					?>
				</select>
				<div class="form-error"><?= form_error('nis'); ?></div>
			</div>
			<div class="form-group">
				<label class="label">Total Pembayaran</label>
				<input id="bayar" name="bayar" value="<?= set_value('bayar'); ?>" type="text" class="form-control"
					placeholder="Contoh : 400000">
				<div class="form-error"><?= form_error('bayar'); ?></div>
			</div>
			<div>
				<div class="form-group">
					<label for="label">Metode Pembayaran</label>
					<input type="text" name="metode" class="form-control" value="Tunai" readonly>
					<div class="form-error"><?= form_error('id_kelas'); ?></div>
				</div>
				<div id="tagihan">
				
				</div>
			</div>
		</div>
		<div class="modal-footer text-right">
			<button type="submit" class="btn btn-outline-primary" id="button">Bayar</button>
		</div>
	</form>
</div>

<div class="tab-pane fade" id="custom-content-below-profile" role="tabpanel" aria-labelledby="custom-content-below-profile-tab" style="padding:10px;">
	<div class="form-group">
		<label class="label">NIS</label>
		<select class="form-control" name="nis" id="nisRiwayat" onchange="get_riwayat()" placeholder="Pilih NIS atau Nama Siswa">
			<option value="" selected>-- Pilih Siswa --</option>
			<?php
			foreach($siswa as $s){?>
			<option value="<?= $s->nis; ?>"><?= $s->nis; ?> - <?= $s->nama_siswa; ?></option>
			<?php
				}
			?>
		</select>
		<table class="table table-stripped table-hover table-bordered text-center">
			<thead>
			<tr>
				<th>NIS</th>
				<th>Nama</th>
				<th>Waktu Pembayaran</th>
				<th>Metode Pembayaran</th>
				<th>Total</th>
				<th>Status</th>
				<th>Petugas</th>
			</tr>
			</thead>
			<tbody id="riwayat">

			</tbody>
		</table>
	</div>
</div>
</div>
</div>
						


<script>
    const flashdata = $('.flash-data').data('flashdata');
        
    switch(flashdata){
        case "berhasil !":
            swal('Pembayaran Berhasil Masuk !' , 'Silahkan Tunggu Email Masuk !' , 'success');
            break;
		case "Gagal":
			swal('Pembayaran Gagal !', 'Pembayaran Gagal Diinput', 'error');
			break;
		case "gagal email !":
			swal('Pembayaran Berhasil Masuk !' , 'Pembayaran Tetap Berhasil, Tapi Email Siswa Tidak Ditemukan' , 'warning');
			break;
    }
</script>
<script>
	$(document).ready(function () {
		$('select').selectize({
			sortField: 'text'
		});
	});
</script>