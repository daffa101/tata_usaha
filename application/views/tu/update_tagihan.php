<div class="flash-data" data-flashdata="<?= $this->session->flashdata('flash'); ?>"></div>
<h2>Form Edit Tagihan</h2>
<hr>
<div class="modal-body">
	<form action="<?= base_url('index.php/Tatausaha/validasi_updatetagihan')?>" method="post">
		<div class="card-body" style="padding:10px;">
          <div class="row">
				<input id="id_tagihan" name="id_tagihan" type="hidden" value="<?= $tagihan->id_tagihan; ?>" class="form-control" readonly>
                <input name="id" type="text" value="<?= $this->uri->segment(3); ?>" class="form-control" readonly>

            <div class="col-md-6">
            <div class="form-group" style="width:370px;">
                <label for="tingkatan">Angkatan</label>
                <select name="id_tingkatan" id="field_tingkatan" class="form-control">
                <option value="<?= $tagihan->id_tingkatan; ?>" selected><?= $tagihan->tingkatan; ?></option>
                <?php
                foreach($tingkatan as $t){ ?>
                    <option value="<?= $t->id_tingkatan; ?>"><?= $t->tingkatan; ?> </option>
                <?php } ?>
                </select><?php echo form_error('id_tingkatan'); ?>
            </div>
            </div>

            <div class="col-md-6">
            <div class="form-group" style="width:370px;">
                <label for="dsp">Biaya DSP</label>
                <input id="field_x_dsp" name="x_dsp" type="text" class="form-control" value="<?= $tagihan->x_dsp ?>" placeholder="Total Biaya DSP " >
                <?php echo form_error('x_dsp'); ?>
            </div>
            </div>

            <div class="col-md-6">
            <div class="form-group" style="width:370px;">
                <label for="seragam">Biaya Seragam</label>
                <input id="field_x_seragam" name="x_seragam" type="text" class="form-control" value="<?= $tagihan->x_seragam ?>" placeholder="Total Biaya Seragam " >
                <?php echo form_error('x_seragam'); ?>
            </div>
            </div>

            <div class="col-md-6">
            <div class="form-group" style="width:370px;">
                <label for="spp10">Biaya SPP Kelas 10</label>
                <input id="field_x_spp" name="x_spp" type="text" class="form-control" value="<?= $tagihan->x_spp ?>" placeholder="Total SPP Kelas 10 " >
                <?php echo form_error('x_spp'); ?>
            </div>
            </div>

            <div class="col-md-6">
            <div class="form-group" style="width:370px;">
                <label for="spp12">Biaya SPP Kelas 11</label>
                <input id="field_xi_spp" name="xi_spp" type="text" class="form-control" value="<?= $tagihan->xi_spp ?>" placeholder="Total SPP Kelas 12 " >
                <?php echo form_error('xi_spp'); ?>
            </div>
            </div>

            <div class="col-md-6">
            <div class="form-group" style="width:370px;">
                <label for="spp12">Biaya SPP Kelas 12</label>
                <input id="field_xii_spp" name="xii_spp" type="text" class="form-control" value="<?= $tagihan->xii_spp ?>" placeholder="Total SPP Kelas 12 " >
                <?php echo form_error('xii_spp'); ?>
            </div>
            </div>
	
            <div class="col-md-6">
            <div class="form-group" style="width:370px;">
                <label for="kunjin">Biaya Kunjin</label>
                <input id="field_xii_kunjin" name="xii_kunjin" type="text" class="form-control" value="<?= $tagihan->xii_kunjin ?>" placeholder="Total Biaya Kunjin " >
                <?php echo form_error('xii_kunjin'); ?>
            </div>
            </div>
            
          </div>
        </div>

            <input name="status" type="hidden" id="status" class="form-control" value="<?= $tagihan->status ?>">


		<div class="modal-footer text-right">
            <button type="submit" class="btn btn-outline-primary" id="button">Simpan</button>
			<a href="<?= base_url('index.php/Tatausaha/tagihan_psb')?>" class="btn btn-outline-danger">Batal</a>
		</div>
	</form>
</div>
<script>
    const flashdata = $('.flash-data').data('flashdata');
        
    switch(flashdata){
        case "berhasil !":
            swal('Pembayaran Berhasil Masuk !' , 'Silahkan Tunggu Email Masuk !' , 'success');
            break;
		case "Gagal":
			swal('Pembayaran Gagal !', 'Pembayaran Gagal Diinput', 'error');
			break;
		case "gagal email !":
			swal('Pembayaran Berhasil Masuk !' , 'Pembayaran Tetap Berhasil, Tapi Email Siswa Tidak Ditemukan' , 'warning');
			break;
    }
</script>
<script>
	$(document).ready(function () {
		$('select').selectize({
			sortField: 'text'
		});
	});
</script>