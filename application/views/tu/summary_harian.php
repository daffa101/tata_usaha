<!-- SUMMARY-->
<div class="card card-primary card-outline">
    <div class="card-header">
      <h3 class="card-title">Ringkasan Hari Ini </h3>

      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
          <i class="fas fa-minus"></i></button>
      </div>
    </div>
    <div class="card-body">
        <div class="row">
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner" style="overflow:auto">
                <h4 class="text-bold">Rp.<?= number_format($all->jml) ?></h4>

                <p>Total Pemasukan</p>
              </div>
              <div class="icon">
                  <i class="fas fa-university"></i>
              </div>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner" style="overflow:auto">
                <h4 class="text-bold"><?= $total_transaksi->jml; ?></h4>

                <p>Total Transaksi</p>
              </div>
              <div class="icon">
                  <i class="fas fa-bars"></i>
              </div>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-warning">
              <div class="inner" style="overflow:auto">
                <h4 class="text-bold"><?= $tunai->jml; ?></h4>

                <p>Transaksi Tunai</p>
              </div>
              <div class="icon">
                <i class="fas fa-balance-scale"></i>
              </div>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-danger">
              <div class="inner" style="overflow:auto">
                <h4 class="text-bold"><?= $transfer->jml; ?></h4>

                <p>Transaksi Transfer</p>
              </div>
              <div class="icon">
                <i class="fas fa-credit-card"></i>
              </div>
            </div>
          </div>
          <!-- ./col -->
        </div>
    </div>
        <div class="tab-content" id="custom-content-below-tabContent">
          <div class="tab-pane fade show active" id="custom-content-below-home" role="tabpanel" aria-labelledby="custom-content-below-home-tab">
              <table class="table table-bordered table-stripped text-center">
                  <thead>
                      <th>No</th>
                      <th>NIS</th>
                      <th>Nama Siswa</th>
                      <th>Kelas</th>
                      <th>Total</th>
                      <th>Metode Bayar</th>
                      <th>Status</th>
                      <th>Nama Petugas</th>
                  </thead>
                  <tbody>
                    <?php
                    $no = 1;
                    foreach($pembayaran as $a):?>
                        <tr>
                            <td><?= $no++ ?></td>
                            <td><?= $a->nis ?></td>
                            <td><?= $a->nama_siswa ?></td>
                            <td><?= $a->nama_kelas ?></td>
                            <td>Rp. <?= number_format($a->total_pembayaran) ?></td>
                            <td><?= $a->metode_pembayaran ?></td>
                            <td><?= $a->status ?></td>
                            <td><?= $a->nama_petugas ?></td>
                        </tr>
                    <?php endforeach; ?>
                    <?php
                    if(empty($pembayaran)){
                        echo "<td colspan='7'>Tidak Ada Data !</td>";
                    }
                    ?>
                  </tbody>
              </table>
          </div>