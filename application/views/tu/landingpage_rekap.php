<style>
    .tab-pane{
        padding: 10px;
        text-align: justify;
    }
</style>
<h2>Rekap</h2><hr/>

<table class="table-sm" style="margin-bottom:20px;">
    <tr>
        <td class="align-middle">Pilih Tanggal :</td>
        <td class="align-middle">
            <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="far fa-calendar-alt"></i>
                  </span>
                </div>
                <input type="text" class="form-control float-right" id="reservation" onchange="ganti_tanggal()">
            </div>
        </td>
    </tr>
</table>

<div id="rekap_bulan">

</div>