<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>
        <?php
        if(isset($title)){
            echo $title;
        }
        else{
            echo "Administrasi Operator";
        }
        ?>
    </title>

    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.1/css/all.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.3.1/css/all.css">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/adminlte.min.css">
    <!-- Css Sendiri -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/style.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <script src='https://cdn.ckeditor.com/ckeditor5/12.1.0/classic/ckeditor.js'></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <!-- SweetAlert2 -->
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <!--Data Table-->
    <link href="<?= base_url('assets/'); ?>vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <?php
    //activate main bar
    function active($a , $b){
        if($a == $b){
            echo "active";
        }
    }
    function menu_open($a , $b){
        if($a == $b){
            echo "menu-open";
        }
    }
    ?>
    <!--nyimpen global var-->
    <script>
        var base_url = "<?= base_url().'index.php/' ?>";
    </script>
    <style>
      .form-error{
          font-size: 13px;
          font-family:Arial;
          color:red;
          font-style:italic;
          margin-bottom: 35px;
      }
    </style>
</head>
<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
      <span class="brand-text font-weight-light">SMKN 11 BANDUNG</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="<?php echo base_url(); ?>assets/dist/img/logo_teacher.png" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">Operator</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview">
            <a href="<?php echo base_url()."index.php/Operator"; ?>" class="nav-link <?php active($active , "Datasiswa"); ?>">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Data Siswa
              </p>
            </a>
          </li>  
            
          <!-- Kelas
          <li class="nav-item has-treeview">
            <a href=" echo base_url()."index.php/Operator/daftar_siswa"; ?>" class="nav-link < active($active , "kelas"); ?>">
                <i class="nav-icon fas fa-users"></i>
                Daftar Siswa
            </a>
          </li> -->
            
          <!--Tahun Ajaran Siswa-->  
          <!-- <li class="nav-item has-treeview">
            <a href="// echo base_url()."index.php/Operator/tahun_ajaran"; ?>" class="nav-link active($active , "tahun_ajaran"); ?>">
                <i class="nav-icon fas fa-calendar-check"></i>
                Tahun Ajaran
            </a>
          </li> -->
          
          <!--Naik Kelas-->
          <!-- <li class="nav-item has-treeview">
            <a href="< echo base_url()."index.php/Operator/wali_kelas"; ?>" class="nav-link < active($active , "naik_kelas"); ?>">
                <i class="nav-icon fas fa-user"></i>
                Wali Kelas
            </a>
          </li> -->

          <!-- Kelas -->
          <!-- <li class="nav-item has-treeview">
            <a href="< echo base_url()."index.php/Operator/get_kelas"; ?>" class="nav-link < active($active , "get_kelas"); ?>">
                <i class="nav-icon fas fa-level-up-alt"></i>
                Kelas
            </a>
          </li> -->

          <li class="nav-item has-treeview">
            <a href="<?php echo base_url("index.php/Login/logout"); ?>" class="nav-link">
              <i class="nav-icon fas fa-sign-out-alt"></i>
              <p>
                Logout
              </p>
            </a>
          </li>
            
            
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <!-- JQUERY-->
  <script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery.min.js"></script>
  <div class="content-wrapper" style="padding:15px;">
      <?php
      $this->load->view($konten);
      ?>
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

</div>
<!-- ./wrapper -->
    <style type="text/css">
        .labelmsg{
            color: red;
        }
    </style>
    <!-- REQUIRED SCRIPTS -->
    <!-- jQuery -->
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
    <!--
    <script>
        $( "#myform" ).validate({
            ignore:[],
          rules: {
            nis: {
              required: true,
              minlength: 3
            },
            nik: {
              required: true,
              minlength: 3
            },
            siswa: {
              required: true,
              minlength: 3
            },
            tanggal_lahir: {
              required: true,
              minlength: 3
            },
            nama_ayah: {
              required: true,
              minlength: 3
            },
            nama_ibu: {
              required: true,
              minlength: 3
            }
          },
        });
    </script>
    -->
    <!-- Bootstrap -->
    <script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- overlayScrollbars -->
    <script src="<?php echo base_url(); ?>assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url(); ?>assets/dist/js/adminlte.js"></script>
    <!-- Page level plugins -->
    <script src="<?= base_url('assets/'); ?>vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="<?= base_url('assets/'); ?>vendor/datatables/dataTables.bootstrap4.min.js"></script>

    <!-- Page level custom scripts -->
    <script src="<?= base_url('assets/'); ?>js/demo/datatables-demo.js"></script>

    <!-- OPTIONAL SCRIPTS -->
    <script src="<?php echo base_url(); ?>assets/dist/js/demo.js"></script>

    <!-- PAGE PLUGINS -->

    <!-- PAGE SCRIPTS -->
    <script src="<?php echo base_url(); ?>assets/dist/js/pages/dashboard2.js"></script>
    <script>
        ClassicEditor
            .create( document.querySelector( '#editor' ) )
            .catch( error => {
                console.error( error );
            } );
    </script>
    <!--CUSTOM JAVASCRIPT / JQUERY-->
    <script src="<?php if(isset($javascript)){echo $javascript; }else{ echo "";} ?>"></script>
</body>
</html>