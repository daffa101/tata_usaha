<h2>Daftar Tahun Ajaran SMKN 11 Bandung</h2><hr>

<div class="flash-data" data-flashdata="<?= $this->session->flashdata('flash'); ?>"></div>
<div class="card card-outline card-primary">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-3">
                <a  href="<?= base_url('index.php/Operator/page_add_ta'); ?>" class="btn btn-outline-primary">+ Tahun Ajaran Baru</a><br>
            </div>
        </div>
        <div class="table">
        <br>
            <table class="table table-stripped table-hover table-bordered">
                <thead>
                    <tr>
                        <td>No</td>
                        <td>Tahun Ajaran</td>
                        <td>Aksi</td>
                    </tr>
                </thead>
                <tbody>
                <?php
                $no = 1;
                foreach($tahun_ajaran as $k):
                ?>
                    <tr>
                        <td><?= $no++; ?></td>
                        <td><?= $k->tahun_ajaran; ?></td>
                        <td class="text-center">
                             <button type="button" data-toggle='modal' data-target='#edit<?= $k->id_tahun_ajaran ?>' class="btn btn-outline-warning btn-sm">Edit</button>
                             <a href="<?= base_url('index.php/Operator/delete_tahun_ajaran/').$k->id_tahun_ajaran; ?>" class="btn btn-outline-danger btn-sm"> Hapus</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="modal fade" id="tahun_ajaran" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Buat Tahun Ajaran Baru</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
            <form action="<?= base_url();?>index.php/Operator/add_tahun_ajaran" method="post">
            <div class="modal-body">
                <div class="card-body" style="padding:0px;">
                    <div class="form-group">
                        <input id="idta" name="idta" type="text" class="form-control" placeholder="Masukkan ID tahun ajaran" value="" required>
                    </div>
                    <div class="form-group">
                        <label for="tahun_ajaran">Tahun Ajaran</label>
                        <input id="tahun_ajaran" name="tahun_ajaran" type="text" class="form-control" placeholder="Masukkan tahun ajaran" required>
                    </div>
                    <div class="form-group">
                        <label for="aktivasi">Aktivasi :</label>
                        <select name="aktivasi" id="select" class="form-control">
                            <option selected disabled>Pilih Aktivasi</option>
                            <?php
                            foreach($listATA as $a){?>
                                <option value="<?= $a ?>"><?= $a ?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="modal-footer text-right">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success">Tambah</button>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>

<?php foreach($tahun_ajaran as $k){ ?>
<div class="modal fade" id="edit<?= $k->id_tahun_ajaran ?>" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit tahun ajaran</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
            <form action="<?= base_url();?>index.php/Operator/edit_tahun_ajaran/<?= $k->id_tahun_ajaran; ?>" method="post">
            <div class="modal-body">
                <div class="card-body" style="padding:0px;">
                    <div class="form-group">
                        <label for="idta">ID Ajaran</label>
                        <input type="text" id="idta" name="idta" value="<?= $k->id_tahun_ajaran ?>" class="form-control" placeholder="Masukkan ID tahun Ajaran" readonly>
                    </div>
                    <div class="form-group">
                        <label for="tahun_ajaran"> Tahun Ajaran</label>
                        <input id="edit_tahun_ajaran" name="edit_tahun_ajaran" type="text" value="<?= $k->tahun_ajaran ?>" class="form-control" placeholder="Masukkan tahun ajaran" required>
                    </div>
                    <!-- <div class="form-group">
                        <label for="aktivasi">Aktivasi :</label>
                        <select name="aktivasi" id="select" class="form-control">
                            <option value="" selected disabled></option>
                           
                        </select>
                    </div> -->
                </div>
                <div class="modal-footer text-right">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success">Simpan</button>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
<?php } ?>

<script>
    const flashdata = $('.flash-data').data('flashdata');
        
    switch(flashdata){
        case "Berhasil !":
            swal('Data Berhasil Masuk !' , 'Data Tahun Ajaran Berhasil Dibuat !' , 'success');
            break;
        case "Diedit !":
            swal('Data Tahun Ajaran Diedit !' , 'Data Tahun Ajaran Berhasil Diedit !' , 'success');
            break;
        case "Dihapus !":
            swal('Data Berhasil Dihapus !' , 'Data Tahun Ajaran Berhasil Dihapus !' , 'success');
            break;
        case "Gagal diedit !":
            swal('Data Gagal Diedit !' , 'Data Tahun Ajaran Gagal Diedit !' , 'error');
            break;
    }
</script>