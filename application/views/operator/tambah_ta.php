<h4 class="modal-title">Buat Tahun Ajaran Baru</h4>
<?php $listATA = ["A","B"]; ?>
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Buat Tahun Ajaran Baru</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>

            <form action="<?= base_url();?>index.php/Operator/add_tahun_ajaran" method="post">
            <div class="modal-body">
                <div class="card-body" style="padding:0px;">
                    <div class="form-group">
                        <input id="idta" name="idta" type="text" class="form-control" placeholder="Masukkan ID tahun ajaran" value="" >
                    </div>
                    <div class="form-group">
                        <label for="tahun_ajaran">Tahun Ajaran</label>
                        <input id="tahun_ajaran" name="tahun_ajaran" type="text" class="form-control" placeholder="Masukkan tahun ajaran" >
                    </div>
                    
                    <?= form_error('tahun_ajaran') ?>
                    <div class="form-group">
                        <label for="aktivasi">Aktivasi :</label>
                        <select name="aktivasi" id="select" class="form-control">
                            <option selected disabled>Pilih Aktivasi</option>
                            <?php
                            foreach($listATA as $a){?>
                                <option value="<?= $a ?>"><?= $a ?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="modal-footer text-right">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success">Tambah</button>
                </div>
            </div>
            </form>
        </div> 
    </div>
</div>