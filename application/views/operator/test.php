<div class="flash-data" data-flashdata="<?= $this->session->flashdata('flash'); ?>"></div>
<div class="card card-primary card-outline">
    <div class="card-header">
        <h3 class="card-title">Tabel Wali Kelas</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
            <i class="fas fa-minus"></i></button>
        </div>
    </div>
    <div class="card-body">
    <div class="row">
            <div class="col-sm-3">
                <button type="button" data-toggle='modal' data-target='#add_wakel' class="btn btn-outline-primary">+ Kelas Baru</button><br>
            </div>
        </div>
    <hr>
        <div class="table">
            <table class="table table-stripped table-bordered table-hover">
                <thead>
                    <tr>
                        <td>NO</td>
                        <td>NAMA KELAS</td>
                        <td>ID TAHUN AJARAN</td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $no = 1;
                    foreach($nama_kelas as $a):
                    ?>
                    <tr>
                        <td><?= $no++;?></td>
                        <td><?= $a->nama_kelas;?></td>
                        <td><?= $a->id_tahun_ajaran;?></td>
                    </tr>
                    <?php endforeach;?>
                    <?php
                        if(count($nama_kelas) == 0):
                    ?>
                    <tr>
                        <td colspan="6" class="text-center">Tidak Ada Data</td>
                    </tr>
                    <?php endif; ?>
                </tbody>
            </table><hr>
        </div>
    </div>
</div>