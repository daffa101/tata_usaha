<h2>Daftar Siswa</h2><hr>

<!-- Biodata -->
<div class="flash-data" data-flashdata="<?= $this->session->flashdata('flash'); ?>"></div>
<div class="card card-primary card-outline">
    <div class="card-header">

        <div class="card-tools">
            <button class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
            <i class="fas fa-minus"></i></button>
        </div>
    </div>
    <div class="card-body">
        <table class="table-sm" style="margin-bottom:20px;">
            <tr>
                <td class="align-middle">Pilih Tahun Ajaran :</td>
                <td class="align-midlle">
                <select onchange="ganti_kelas()" id="select" class="form-control">
                    <option selected disabled>Pilih Tahun Ajaran...</option>
                    <?php
                    foreach($tahun_ajaran as $a){?>
                        <option value="<?= $a->id_tahun_ajaran ?>"><?= $a->tahun_ajaran ?></option>
                    <?php
                    }
                    ?>
                </select>
                </td>
                <td><a href="<?= base_url('index.php/operator/page_add_siswa/') ?>" class="btn btn-outline-primary">+ Tambah Siswa</a></td>
            </tr>
        </table><hr>
        <div class="table">
            <table class="table table-striped table-bordered table-hover">
            <thead>
                <tr>
                    <td class="text-center">No</td>
                    <td class="text-center">Nama Kelas</td>
                    <td class="text-center">Tahun Ajaran</td>
                    <td class="text-center">Aksi</td>
                </tr>
            </thead>
            <tbody id="tahun_kelas">
                
            </tbody>
            </table>
        </div>
    </div>
    </div>
</div>