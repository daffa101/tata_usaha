<?= form_open('Operator/validasi_siswa'); ?>
<div class="modal-body">
    <div class="card-body" style="padding:0px;">

        <div class="form-group">
            <label class="label">NIS</label>
            <input id="nis" name="nis" type="text" value="<?= set_value('nis'); ?>" class="form-control" placeholder="Masukkan NIS Siswa">
            <div class="form-error"><?= form_error('nis'); ?></div>
        </div>
        <div class="form-group">
            <label class="label">Nama Siswa</label>
            <input id="nama_siswa" name="nama_siswa" value="<?= set_value('nama_siswa'); ?>" type="text" class="form-control" placeholder="Masukkan Nama Siswa" >
            <div class="form-error"><?= form_error('nama_siswa'); ?></div>
        </div>
        <div>
            <div class="form-group">
            <label for="label">Kelas</label>
            <select class="form-control" name="id_kelas" id="id_kelas">
                <option value="<?= set_value('id_kelas'); ?>" selected disabled>-- Pilih Kelas --</option>
                <?php
                foreach($daftar_kelas as $k){?>
                <option value="<?= $k->id_kelas; ?>"><?= $k->nama_kelas; ?></option>
                <?php
                }
                ?>
            </select>
            <div class="form-error"><?= form_error('id_kelas'); ?></div>
            </div>
        </div>
        <div class="form-group">
            <label for="jenis_kelamin">Jenis Kelamin</label>
            <div class="form-check">
                <input class="form-check-input" name="jk" type="radio" value="P" >
                <label class="form-check-label">Perempuan</label>
                <span class="checkmark"></span>
            </div>
            <div class="form-check">
                <input class="form-check-input" name="jk" type="radio" value="L" >
                <label class="form-check-label">Laki - Laki</label>
                <span class="checkmark"></span>
            </div>
            <div class="form-error"><?= form_error('jk'); ?></div> 
        </div>
        <div class="form-group">
            <label for="no_telepon">No Telepon</label>
            <input id="no_telepon" name="no_telepon" type="text" value="<?= set_value('no_telepon'); ?>" class="form-control" placeholder="Masukkan No Telepon">
            <div class="form-error"><?= form_error('no_telepon'); ?></div>
        </div>
    </div>
    <div class="modal-footer text-right">
        <a type="button" class="btn btn-default" href="<?= base_url('index.php/Operator/daftar_siswa')?>">Batal</a>
        <button type="submit" class="btn btn-success">Tambah</button>
    </div>
</div>
</form>