<!-- Biodata Siswa -->
<div class="flash-data" data-flashdata="<?= $this->session->flashdata('flash'); ?>"></div>
<div class="card card-primary card-outline">
    <div class="card-header">
        <h3 class="card-title">Tabel Wali Kelas</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
            <i class="fas fa-minus"></i></button>
        </div>
    </div>
    <div class="card-body">
    <div class="row">
            <div class="col-sm-3">
                <button type="button" data-toggle='modal' data-target='#add_wakel' class="btn btn-outline-primary">+ Kelas Baru</button><br>
            </div>
        </div>
        <hr>
        <div class="card shadow mb-4">
            <div class="card-body">
                <div class="table-responsive">
                <table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>NAMA WALI KELAS</th>
                        <th>KELAS</th>
                        <th>AKSI</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $no = 1;
                    foreach($wali_kelas as $a):
                    ?>
                    <tr>
                        <td><?= $no++;?></td>
                        <td><?= $a->nama_walikelas;?></td>
                        <td><?= $a->nama_kelas;?></td>
                        <td class="text-center">
                            <button type="button" data-toggle='modal' data-target='#edit<?= $a->id_walikelas; ?>' class="btn btn-outline-primary btn-sm"> Edit Data</button>
                            <a href="<?= base_url('index.php/Operator/delete_wakel/').$a->id_walikelas; ?>" class="btn btn-outline-danger btn-sm"> Hapus</a>
                        </td>
                    </tr>
                    <?php endforeach;?>
                    <?php
                        if(count($wali_kelas) == 0):
                    ?>
                    <tr>
                        <td colspan="6" class="text-center">Tidak Ada Data</td>
                    </tr>
                    <?php endif; ?>
                </tbody>
            </table><hr>
        </div>
    </div>
</div>

<div class="modal fade" id="add_wakel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tambah Wali Kelas</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
            <form action="<?= base_url();?>index.php/Operator/add_wakel" method="post">
            <div class="modal-body">
                <div class="card-body" style="padding:0px;">
                    <div class="form-group">
                        <input id="idw" name="idw" type="hidden" class="form-control" placeholder="Masukkan ID Wali Kelas" required>
                    </div>
                    <div class="form-group">
                        <label for="nip">NIP</label>
                        <input id="nip" name="nip" type="text" class="form-control" placeholder="Masukkan NIP Wali Kelas">
                    </div>
                    <div class="form-group">
                        <label for="nama_walikelas">Nama Wali Kelas</label>
                        <input id="nama_walikelas" name="nama_walikelas" type="text" class="form-control" placeholder="Masukkan Nama Wali Kelas" required>
                    </div>
                    <div class="form-group">
                        <label for="kelas">Kelas :</label>
                        <select name="kelas" id="select" class="form-control">
                            <option selected disabled>Pilih Kelas</option>
                            <?php
                            foreach($kelas as $b){?>
                                <option value="<?= $b->id_kelas; ?>"><?= $b->nama_kelas; ?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="modal-footer text-right">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success">Tambah</button>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>

<?php foreach($wali_kelas as $a){ ?>
<div class="modal fade" id="edit<?= $a->id_walikelas ?>" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit Data Wali Kelas</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
            <form action="<?= base_url();?>index.php/Operator/edit_wakel/<?= $a->id_walikelas; ?>" method="post">
            <div class="modal-body">
                <div class="card-body" style="padding:0px;">
                    <div class="form-group">
                        <label for="nip">NIP</label>
                        <input type="text" name="edit_nip" value="<?= $a->nip ?>" class="form-control" placeholder="Masukkan NIP Wali Kelas">
                        <input type="hidden" name="id_walikelas" value="<?= $a->id_walikelas; ?>">
                    </div>
                    <div class="form-group">
                        <label for="nama_walikelas">Nama Wali Kelas</label>
                        <input id="edit_nama_walikelas" name="edit_nama_walikelas" type="text" value="<?= $a->nama_walikelas ?>" class="form-control" placeholder="Masukkan Nama Siswa" required>
                    </div>
                    <div class="form-group">
                        <label for="edit_kelas">Kelas</label>
                        <select name="edit_kelas" id="edit_kelas" class="form-control">
                            <option value="<?= $a->id_kelas; ?>" selected><?= $a->nama_kelas; ?></option>
                            <?php
                                foreach($kelas as $b):
                            ?>
                            <option value="<?= $b->id_kelas; ?>"><?= $b->nama_kelas; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="modal-footer text-right">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success">Simpan</button>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
<?php } ?>

<script>
    const flashdata = $('.flash-data').data('flashdata');
        
    switch(flashdata){
        case "Berhasil !":
            swal('Data Berhasil Masuk !' , 'Data Wali Kelas Berhasil Dibuat !' , 'success');
            break;
        case "Diedit !":
            swal('Data Wali Kelas Diedit !' , 'Data Wali Kelas Berhasil Diedit !' , 'success');
            break;
        case "Dihapus !":
            swal('Data Berhasil Dihapus !' , 'Data Kelas Berhasil Dihapus !' , 'success');
            break;
        case "Gagal diedit !":
            swal('Data Gagal Diedit !' , 'Data Wali Kelas Gagal Diedit !' , 'error');
            break;
        case "Gagal !":
            swal('Data Gagal Dihapus !', 'Data Wali Kelas Gagal Dihapus !', 'error');
            break;
    }
</script>