<h2>Daftar Kelas SMKN 11 Bandung</h2><hr>

<div class="flash-data" data-flashdata="<?= $this->session->flashdata('flash'); ?>"></div>
<div class="card card-outline card-primary">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-3">
                <button type="button" data-toggle='modal' data-target='#kelas' class="btn btn-outline-primary">+ Kelas Baru</button><br>
            </div>
        </div>
        <div class="table">
        <br>
            <table class="table table-stripped table-hover table-bordered">
                <thead>
                    <tr>
                        <td>No</td>
                        <td>Nama Kelas</td>
                        <td>Tahun Ajaran</td>
                        <td>Aksi</td>
                    </tr>
                </thead>
                <tbody>
                <?php
                $no = 1;
                foreach($kelas as $k):
                ?>
                    <tr>
                        <td><?= $no++; ?></td>
                        <td><?= $k->nama_kelas; ?></td>
                        <td><?= $k->tahun_ajaran; ?></td>
                        <td class="text-center">
                             <button type="button" data-toggle='modal' data-target='#edit<?= $k->id_kelas ?>' class="btn btn-outline-warning">Edit</button>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="modal fade" id="kelas" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Buat Kelas Baru</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
            <form action="<?= base_url();?>index.php/Operator/add_kelas" method="post">
            <div class="modal-body">
                <div class="card-body" style="padding:0px;">
                    <div class="form-group">
                        <label for="id_kelas"></label>
                        <input id="idk" name="idk" type="text" class="form-control" placeholder="Masukkan ID Kelas" required>
                    </div>
                    <div class="form-group">
                        <label for="nama_kelas">Nama Kelas</label>
                        <input id="nama_kelas" name="nama_kelas" type="text" class="form-control" placeholder="Masukkan Nama Kelas" required>
                    </div>
                    <div class="form-group">
                        <label for="tahun_ajaran">Tahun Ajaran :</label>
                        <select name="tahun_ajaran" id="select" class="form-control">
                            <option selected disabled>Pilih Tahun Ajaran</option>
                            <?php
                            foreach($tahun_ajaran as $a){?>
                                <option value="<?= $a->id_tahun_ajaran ?>"><?= $a->tahun_ajaran ?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="modal-footer text-right">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success">Tambah</button>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>

<?php foreach($kelas as $k){ ?>
<div class="modal fade" id="edit<?= $k->id_kelas ?>" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit Kelas</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
            <form action="<?= base_url();?>index.php/Operator/edit_kelas/<?= $k->id_kelas; ?>" method="post">
            <div class="modal-body">
                <div class="card-body" style="padding:0px;">
                    <div class="form-group">
                        <input type="hidden" name="edit_id_kelas" value="<?= $k->id_kelas ?>" class="form-control" placeholder="Masukkan ID Kelas">
                    </div>
                    <div class="form-group">
                        <label for="nama_kelas">Nama Kelas</label>
                        <input id="edit_nama_kelas" name="edit_nama_kelas" type="text" value="<?= $k->nama_kelas ?>" class="form-control" placeholder="Masukkan Nama Kelas" required>
                    </div>
                    <div class="form-group">
                        <label for="tahun_ajaran">Tahun Ajaran</label>
                        <select name="edit_tahun_ajaran" id="edit_tahun_ajaran" class="form-control">
                            <option value="<?= $k->id_tahun_ajaran ?>" selected><?= $k->tahun_ajaran ?></option>
                            <?php
                            foreach($tahun_ajaran as $t){
                            ?>
                            <option value="<?= $t->id_tahun_ajaran ?>"><?= $t->tahun_ajaran ?></option>
                            <?php }?>
                        </select>
                    </div>
                </div>
                <div class="modal-footer text-right">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success">Simpan</button>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
<?php } ?>

<script>
    const flashdata = $('.flash-data').data('flashdata');
        
    switch(flashdata){
        case "Berhasil !":
            swal('Data Berhasil Masuk !' , 'Data Siswa Berhasil Dibuat !' , 'success');
            break;
        case "Diedit !":
            swal('Data Kelas Diedit !' , 'Data Kelas Berhasil Diedit !' , 'success');
            break;
        case "Dihapus !":
            swal('Data Berhasil Dihapus !' , 'Data Kelas Berhasil Dihapus !' , 'success');
            break;
        case "Gagal diedit !":
            swal('Data Gagal Diedit !' , 'Data Kelas Gagal Diedit !' , 'error');
            break;
    }
</script>