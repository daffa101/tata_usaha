<!-- Biodata Siswa -->
<div class="flash-data" data-flashdata="<?= $this->session->flashdata('flash'); ?>"></div>
<div class="card card-primary card-outline">
    <div class="card-header">
        <h3 class="card-title">Tabel Detail Kelas</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
            <i class="fas fa-minus"></i></button>
        </div>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-sm-3">
                
            </div>
        </div><hr>
        <div class="table">
            <table class="table table-stripped table-bordered table-hover">
                <thead>
                    <tr>
                        <td>NO</td>
                        <td>NIS</td>
                        <td>NAMA SISWA</td>
                        <td>JENIS KELAMIN</td>
                        <td>NO TELEPON</td>
                        <td>AKSI</td>
                    </tr>
                </thead>
                <tbody>
                <?php
                $no = 1;
                foreach($detail_siswa as $a):
                ?>
                    <tr>
                        <td><?= $no++;?></td>
                        <td><?= $a->nis;?></td>
                        <td><?= $a->nama_siswa;?></td>
                        <td><?= $a->jk;?></td>
                        <td><?= $a->no_telp;?></td>
                        <td class="text-center">
                            <button type="button" data-toggle='modal' data-target='#edit<?= $a->nis?>' class="btn btn-outline-primary btn-sm"> Edit Data</button>
                            <a href="<?= base_url('index.php/Operator/delete_data/').$a->id_kelas.'/'.$a->nis?>" class="btn btn-outline-danger btn-sm"> Hapus</a>
                        </td>
                    </tr>
                    <?php endforeach;?>
                    <?php
                        if(count($detail_siswa) == 0):
                    ?>
                    <tr>
                        <td colspan="6" class="text-center">Tidak Ada Data</td>
                    </tr>
                    <?php endif; ?>
                </tbody>
            </table><hr>
            <div class="row">
                <div class="col-sm-2">
                    <a href="<?= base_url('index.php/Operator/daftar_siswa')?>" class="btn btn-outline-danger btn-sm btn-block">Kembali</a>
                </div>
            </div>
        </div>
    </div>
</div>

<?php foreach($detail_siswa as $a){ ?>
<div class="modal fade" id="edit<?= $a->nis ?>" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit Data Siswa</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
            <form action="<?= base_url();?>index.php/Operator/edit_siswa/<?= $a->id_kelas; ?>/<?= $a->nis; ?>" method="post">
            <div class="modal-body">
                <div class="card-body" style="padding:0px;">
                    <div class="form-group">
                        <label for="nis">NIS</label>
                        <input type="text" value="<?= $a->nis ?>" class="form-control" placeholder="Masukkan NIS Siswa" disabled>
                    </div>
                    <div class="form-group">
                        <label for="nama_siswa">Nama Siswa</label>
                        <input id="edit_nama_siswa" name="edit_nama_siswa" type="text" value="<?= $a->nama_siswa ?>" class="form-control" placeholder="Masukkan Nama Siswa" required>
                    </div>
                    <div class="form-group">
                        <label for="jenis_kelamin">Jenis Kelamin</label>
                        <div class="form-check">
                            <input class="form-check-input" name="edit_jk" type="radio" value="P" <?php if(($a->jk) == "P"){ echo 'checked';} ?>>
                            <label class="form-check-label">Perempuan</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" name="edit_jk" type="radio" value="L" <?php if(($a->jk) == "L"){ echo 'checked';} ?>>
                            <label class="form-check-label">Laki - Laki</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="no_telepon">No Telepon</label>
                        <input id="edit_no_telepon" name="edit_no_telepon" type="text" value="<?= $a->no_telp ?>" class="form-control" placeholder="Masukkan No Telepon">
                    </div>
                </div>
                <div class="modal-footer text-right">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success">Simpan</button>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
<?php } ?>

<script>
    const flashdata = $('.flash-data').data('flashdata');
        
    switch(flashdata){
        case "Berhasil !":
            swal('Data Berhasil Masuk !' , 'Data Siswa Berhasil Dibuat !' , 'success');
            break;
        case "Diedit !":
            swal('Data Siswa Diedit !' , 'Data Siswa Berhasil Diedit !' , 'success');
            break;
        case "Dihapus !":
            swal('Data Berhasil Dihapus !' , 'Data Kelas Berhasil Dihapus !' , 'success');
            break;
        case "Gagal diedit !":
            swal('Data Gagal Diedit !' , 'Data Siswa Gagal Diedit !' , 'error');
            break;
        case "Gagal !":
            swal('Data Gagal Dihapus !', 'Data Siswa Gagal Dihapus !', 'error');
            break;
    }
</script>