<?php

class Operator extends CI_Controller{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_Operator');
        $this->load->library('form_validation');
        $this->load->helper(array('form', 'url'));

        if($this->session->userdata('status') != "login")
        {
            redirect(base_url("index.php/Login"));
        }
        if($this->session->userdata('jabatan') != "operator")
        {
            redirect(base_url("index.php/Login"));
        }
    }

    public function index()
    {
        // Wajib tiap function
        $data['kelas'] = $this->M_Operator->nav_kelas();

        $data['title'] = "Administrasi | Operator";
        // $data['nama_kelas'] = $this->M_Operator->get_grade()->result();

        //active bar
        $data['active'] = "Datasiswa";
        $data['menu_open'] = "nothing";

        $data['konten'] = "operator/import_siswa";
        $data['kelas'] = $this->M_Operator->nav_kelas();
        $this->load->view('operator/header_footer', $data);
    }

    public function daftar_siswa()
    {
        // Wajib tiap function
        $data['kelas'] = $this->M_Operator->nav_kelas();

        $data['title'] = "Operator | Kelas Siswa";
        $data['active'] = "kelas";

        // Data kelas dan siswa
        $data['tahun_ajaran'] = $this->M_Operator->getTahunAjaran()->result();

        $data['menu_open'] = "nothing";
        $data['konten'] = "operator/mainpage_siswa";

        $data['javascript'] = base_url()."assets/js_custom/op_kelas.js";
        $this->load->view("operator/header_footer", $data);
    }

    public function page_add_siswa(){
        $data['kelas'] = $this->M_Operator->nav_kelas();

        $data['title'] = "Operator | Tambah Siswa";
        $data['active'] = "kelas";

        $data['tahun_ajaran'] = $this->M_Operator->getTahunAjaran()->result();
        $data['daftar_kelas'] = $this->M_Operator->getAllKelas()->result();
        
        $data['menu_open'] = "nothing";
        $data['konten'] = "operator/tambah_siswa";

        $data['javascript'] = base_url()."assets/js/op_kelas.js";
        $this->load->view("operator/header_footer", $data);
    }

    public function wali_kelas()
    {
        $data['kelas'] = $this->M_Operator->nav_kelas();

        $data['title'] = "Operator | Wali Kelas";
        $data['active'] = "wali_kelas";

        // Get Data
        $data['wali_kelas'] = $this->M_Operator->getWakel()->result();
        $data['kelas'] = $this->M_Operator->getAllKelas()->result();

        $data['menu_open'] = "nothing";
        $data['konten'] = "operator/page_wakel";

        $data['javascript'] = base_url()."assets/js_custom/op_kelas.js";
        $this->load->view("operator/header_footer", $data);
    }

    public function detail_kelas(){
        // Wajib tiap function
        $data['nav_kelas'] = $this->M_Operator->nav_kelas();

        // Get data kelas
        $data['kelas'] = $this->M_Operator->getKelas()->row();

        // Get data siswa
        $data['detail_siswa'] = $this->M_Operator->getSiswa()->result();

        $data['title'] = "Operator | Detail Kelas Siswa";

        $data['active'] = "kelas";
        $data['menu_open'] = "nothing";

        $data['konten'] = 'operator/detail_kelas';
        $this->load->view('operator/header_footer', $data);
    }
    
    public function page_add_ta(){
        $data['nav_kelas'] = $this->M_Operator->nav_kelas();
        
        $data['title'] = "Operator | Tambah Tahun Ajaran";

        $data['active'] = "tahun_ajaran";
        $data['menu_open'] = "nothing";

        $data['konten'] = 'operator/tambah_ta';
        $this->load->view('operator/header_footer', $data);
    }

    public function get_kelas(){
        // Wajib tiap function
        $data['nav_kelas'] =  $this->M_Operator->nav_kelas();

        // Get data kelas
        $data['kelas'] = $this->M_Operator->getAllKelas()->result();
        $data['tahun_ajaran'] = $this->M_Operator->getAllTH()->result();
 
        $data['title'] = "Operator | Daftar Kelas";

        $data['active'] = "get_kelas";
        $data['menu_open'] = "nothing";

        // load konten
        $data['konten'] = 'operator/daftar_kelas';
        $this->load->view('operator/header_footer', $data);
    }

    public function validasi_siswa(){
        $id_kelas = $this->uri->segment(3);

        $this->form_validation->set_rules('nis', 'NIS', 'required|numeric|min_length[10]',
        array('required'=>'*Tolong Isi Kolom NIS'));

        $this->form_validation->set_rules('nama_siswa', 'Nama Siswa', 'required',
        array('required'=>'*Tolong Isi Kolom Nama'));

        $this->form_validation->set_rules('id_kelas', 'Kelas', 'required',
        array('required'=>'*Tolong Pilih Kelas'));

        $this->form_validation->set_rules('no_telepon', 'No Telepon', 'numeric|min_length[10]');

        $this->form_validation->set_rules('jk', 'Jenis Kelamin', 'required',
        array('required'=>'*Tolong Pilih Gender !'));

        if($this->form_validation->run() == FALSE){
            $data['kelas'] = $this->M_Operator->nav_kelas();

            $data['title'] = "Operator | Tambah Siswa";
            $data['active'] = "kelas";

            $data['tahun_ajaran'] = $this->M_Operator->getTahunAjaran()->result();
            $data['daftar_kelas'] = $this->M_Operator->getAllKelas()->result();
            $data['id_kelas'] = $this->input->post('idk');

            $data['menu_open'] = "nothing";
            $data['konten'] = "operator/tambah_siswa";

            $data['javascript'] = base_url()."assets/js/op_kelas.js";
            $this->load->view("operator/header_footer", $data);
        }else{
            $nis = $this->input->post('nis');
            $nama = $this->input->post('nama_siswa');
            $jk = $this->input->post('jk');
            $id_kelas = $this->input->post('id_kelas');
            $no_telpon = $this->input->post('no_telepon');

            // validasi
            if($this->M_Operator->add_siswa($nis,$nama,$jk,$id_kelas,$no_telpon)){
                $this->session->set_flashdata('flash', 'Berhasil !');
                redirect(base_url("index.php/Operator/detail_kelas/$id_kelas"));
            }else{
                $this->session->set_flashdata('flash' , 'Gagal !');
                redirect(base_url("index.php/Operator/detail_kelas/$id_kelas"));
            }
        }
    }

    public function load_kelas($id_tahun_ajaran){
        $data_kelas = $this->M_Operator->return_result("SELECT * FROM kelas , tahun_ajaran WHERE kelas.`id_tahun_ajaran` = tahun_ajaran.`id_tahun_ajaran` AND kelas.`id_tahun_ajaran` = '$id_tahun_ajaran' ORDER BY kelas.`id_kelas` ASC");
        $no = 1;
        foreach($data_kelas as $a){?>
            <tr>
                <td><?= $no++; ?></td>
                <td><?= $a->nama_kelas; ?></td>
                <td><?= $a->tahun_ajaran?></td>
                <td> <a href="<?= base_url();?>index.php/Operator/detail_kelas/<?= $a->id_kelas; ?>" class="btn btn-outline-primary btn-sm btn-block">Detail</a> </td>
            </tr>
        <?php
        }
    }

    public function add_kelas(){
        $data = array(
            "id_kelas" => $this->input->post('idk'),
            "id_tahun_ajaran" => $this->input->post('tahun_ajaran'),
            "nama_kelas" => $this->input->post('nama_kelas')
        );

        if($this->db->insert('kelas', $data)){
            $this->session->set_flashdata('flash', 'Berhasil !');
            redirect(base_url("index.php/Operator/get_kelas"));
        }else{
            $this->session->set_flashdata('flash', 'Gagal !');
            redirect(base_url("index.php/Operator/get_kelas"));
        }
    }

    public function add_siswa(){
        // Get id kelas
        $id_kelas = $this->uri->segment(3);
        $nis = $this->input->post('nis');
        $nama = $this->input->post('nama_siswa');
        $jk = $this->input->post('jk');
        $no_telpon = $this->input->post('no_telepon');

        // validasi
        if($this->M_Operator->add_siswa($nis,$nama,$jk,$id_kelas,$no_telpon)){
            $this->session->set_flashdata('flash', 'Berhasil !');
            redirect(base_url("index.php/Operator/detail_kelas/$id_kelas"));
        }else{
            $this->session->set_flashdata('flash' , 'Gagal !');
            redirect(base_url("index.php/Operator/detail_kelas/$id_kelas"));
        }
    }

    public function add_wakel(){
        $data = array(
            "id_walikelas" => $this->input->post('idw'),
            "nama_walikelas" => $this->input->post('nama_walikelas'),
            "nip" => $this->input->post('nip'),
            "id_kelas" => $this->input->post('kelas')
        );

        if($this->M_Operator->insert_wakel($data)){
            $this->session->set_flashdata('flash', 'Berhasil !');
            redirect(base_url("index.php/Operator/wali_kelas"));
        }else{
            $this->session->set_flashdata('flash', 'Gagal !');
            redirect(base_url("index.php/Operator/wali_kelas"));
        }
    }

    //buat delete wakel
    public function delete_wakel($where){
        $id = $this->uri->segment(3);
        $where = array("id_walikelas" => $id);
        
        if($this->M_Operator->delete_wk($where)){
            $this->session->set_flashdata('flash', 'Dihapus !');
            redirect(base_url("index.php/Operator/page_wakel"));
        }else{
            $this->session->set_flashdata('flash', 'Dihapus !');
            redirect(base_url("index.php/Operator/page_wakel"));
        }
        
    }


    public function edit_siswa(){
        // get id kelas sama nis
        $id_kelas = $this->uri->segment(3);
        $nis = $this->uri->segment(4);

        // data yang di edit
        $data_edit = array(
            'nama_siswa' => $this->input->post('edit_nama_siswa'),
            'jk' => $this->input->post('edit_jk'),
            'no_telp' => $this->input->post('edit_no_telepon')
        );

        // validasi ke edit atau engga
        if($this->M_Operator->update_data($nis, $data_edit, 'nis', 'siswa')){
            $this->session->set_flashdata('flash', 'Diedit !');
            redirect(base_url("index.php/Operator/detail_kelas/$id_kelas"));
        }else{
            $this->session->set_flashdata('flash', 'Gagal Diedit !');
            redirect(base_url("index.php/Operator/detail_kelas/$id_kelas"));
        }
    }

    public function delete_data(){
        $id_kelas = $this->uri->segment(3);
        $nis = $this->uri->segment(4);

        $where = array("nis" => $nis);

        if($this->M_Operator->delete_siswa($where,'siswa')){
            $this->session->set_flashdata('flash', 'Dihapus !');
            redirect(base_url("index.php/Operator/detail_kelas/$id_kelas"));
        }else{
            $this->session->set_flashdata('flash', 'Dihapus !');
            redirect(base_url("index.php/Operator/detail_kelas/$id_kelas"));
        }
        
    }

    public function edit_kelas(){
        $nama_kelas = $this->input->post('edit_nama_kelas');
        $tahun_ajaran = $this->input->post('edit_tahun_ajaran');
        $id_kelas = $this->input->post('edit_id_kelas');

        $data = array(
            'nama_kelas' => $nama_kelas,
            'id_tahun_ajaran' => $tahun_ajaran
        );

        $where = array(
            'id_kelas' => $id_kelas
        );

        if($this->M_Operator->edit_kelas($where,$data,'kelas')){
            $this->session->set_flashdata('flash', 'Diedit !');
            redirect(base_url("index.php/Operator/get_kelas"));
        }else{
            $this->session->set_flashdata('flash', 'Gagal Diedit !');
            redirect(base_url("index.php/Operator/get_kelas"));
        }
    }

    public function edit_wakel(){
        $nip = $this->input->post('edit_nip');
        $id_walikelas = $this->input->post('id_walikelas');
        $nama_walikelas = $this->input->post('edit_nama_walikelas');
        $id_kelas = $this->input->post('edit_kelas');

        $data = array(
            'nip' => $nip,
            'nama_walikelas' => $nama_walikelas,
            'id_kelas' => $id_kelas
        );

        $where = array(
            'id_walikelas' => $id_walikelas
        );

        if($this->M_Operator->update_wakel($where,$data,'wali_kelas')){
            $this->session->set_flashdata('flash', 'Diedit !');
            redirect(base_url("index.php/Operator/wali_kelas"));
        }else{
            $this->session->set_flashdata('flash', 'Gagal Diedit !');
            redirect(base_url("index.php/Operator/wali_kelas"));
        }
    }

    //tahun ajaran
    public function tahun_ajaran(){
        $data['tahun_ajaran'] = $this->M_Operator->getTahunAjaran()->result();
 
        $data['title'] = "Operator | Daftar Tahun Ajaran";

        $data['active'] = "tahun_ajaran";
        $data['menu_open'] = "nothing";

        // load konten
        $data['konten'] = 'operator/daftar_tahun_ajaran';
        $this->load->view('operator/header_footer', $data);
    }
//validasi tahun ajaran

    public function add_tahun_ajaran(){

        $this->load->helper(array('form','url'));
        $this->load->library('form_validation');

        $validation =  $this->form_validation;
        $validation->set_rules('tahun_ajaran','Masukkan tahun ajaran','required|numeric',
        array('required'=>'*Tolong isi tahun ajaran',
                'numeric'=>'Tolong isi dengan angka'));
            if ($validation->run() == FALSE) {
                //$data['nav_kelas'] = $this->M_Operator->nav_kelas();
                // redirect(base_url("index.php/Operator/page_add_ta"));
                // echo "gagal";
                $data['nav_kelas'] = $this->M_Operator->nav_kelas();
        
            $data['title'] = "Operator | Tambah Tahun Ajaran";

            $data['active'] = "tahun_ajaran";
            $data['menu_open'] = "nothing";

            $data['konten'] = 'operator/tambah_ta';
            $this->load->view('operator/header_footer', $data);

             } else {

        $data = array(
            "id_tahun_ajaran" => $this->input->post('idta'),
            "tahun_ajaran" => $this->input->post('tahun_ajaran'),
            "aktivasi" => $this->input->post('aktivasi')
        );

        if($this->M_Operator->insertTA($data)){
            $this->session->set_flashdata('flash', 'Berhasil !');
            redirect(base_url("index.php/Operator/tahun_ajaran"));
        }else{
            $this->session->set_flashdata('flash', 'Gagal !');
            redirect(base_url("index.php/Operator/tahun_ajaran"));
            }
        }
    }

    public function edit_tahun_ajaran(){
        $data = array(
            "tahun_ajaran" => $this->input->post('edit_tahun_ajaran'),
            "aktivasi" => $this->input->post('aktivasi')
        );
        $this->db->where("id_tahun_ajaran",$this->input->post('idta'));
        if($this->db->update('tahun_ajaran',$data)){
            $this->session->set_flashdata('flash', 'Diedit !');
            redirect(base_url("index.php/Operator/tahun_ajaran"));
        }else{
            $this->session->set_flashdata('flash', 'Gagal Diedit !');
            redirect(base_url("index.php/Operator/tahun_ajaran"));
        }
    }

    public function delete_tahun_ajaran($idta){
        
        if($this->M_Operator->delete_mta($idta)){
            $this->session->set_flashdata('flash', 'Dihapus !');
            redirect(base_url("index.php/Operator/tahun_ajaran"));
        }else{
            $this->session->set_flashdata('flash', 'Dihapus !');
            redirect(base_url("index.php/Operator/tahun_ajaran"));
        }
        
    }


    //untuk import data excel
    public function import(){
        $this->M_Operator->model_import();
    }

}

?>