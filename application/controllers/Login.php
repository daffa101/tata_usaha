<?php

class Login extends CI_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->model("M_Operator");
        $this->load->model('M_Tu');
    }

    public function index(){
        $this->load->view("login");
    }

    public function proses_login(){
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $where = array(
            'username' => $username,
            'password' => md5($password)
        );

        $cek = $this->M_Operator->login("akun_karyawan", $where)->num_rows();
        if($cek > 0){
            $query = "SELECT * FROM akun_karyawan, karyawan WHERE akun_karyawan.`nik` = karyawan.`nik` AND username = '$username' AND password = '".md5($password)."'";
            $data_akun = $this->M_Operator->return_result($query);

            foreach($data_akun as $a){
                $username = $a->username;
                $nik = $a->nik;
                $nama = $a->nik;
                $jabatan = $a->jabatan;
                $nama_petugas = $a->nama_petugas;
            }

            $data_session = array(
                'username' => $username,
                'nama' => $nama,
                'nik' => $nik,
                'jabatan' => $jabatan,
                'nama_petugas' => $nama_petugas,
                'status' => "login"
            );

            switch($a->jabatan){
                case "tata usaha":
                    $this->session->set_userdata($data_session);
                    redirect(base_url("index.php/TataUsaha"));
                break;
                case "operator":
                    $this->session->set_userdata($data_session);
                    redirect(base_url("index.php/Operator"));
                break;
            }
        }else{
            echo "Error: Hubungi Admin !";
        }
    }

    function logout(){
        $this->session->unset_userdata('username');
		$this->session->unset_userdata('nama');
        $this->session->unset_userdata('status');
        redirect(base_url("index.php/Login"));
    }
}

?>