<?php

class Tatausaha extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('M_Tu');
        $this->load->library('form_validation');
        $this->load->helper(array('form', 'url'));

        if($this->session->userdata('status') != "login")
        {
            redirect(base_url("index.php/Login"));
        }
        if($this->session->userdata('jabatan') != "tata usaha")
        {
            redirect(base_url("index.php/Login"));
        }

        $this->load->helper('download');
    }

    public function index(){

        // awal
        $data['title'] = "Administrasi | Tata Usaha";

        // active bar
        $data['active'] = "dashboard";
        $data['javascript'] = base_url().'assets/js/tu_psb.js';
        $data['menu_open'] = "nothing";

        $data['konten'] = "tu/landingpage";
        $this->load->view('tu/header_footer', $data);

    }

    // Main Content Dashboard
    public function load_harian($tanggal){
        $data_tanggal = explode("%20-%20" , $tanggal);
        
        $tanggal_awal = explode("-" , $data_tanggal[0]);
        $tanggal_awal = $tanggal_awal[2]."-".$tanggal_awal[0]."-".$tanggal_awal[1];
        
        $tanggal_akhir = explode("-" , $data_tanggal[1]);
        $tanggal_akhir = $tanggal_akhir[2]."-".$tanggal_akhir[0]."-".$tanggal_akhir[1];

        // Data Summary kotak kotak
        $data['transfer'] = $this->M_Tu->get_transfer($tanggal_awal, $tanggal_akhir)->row();
        $data['transfer']->jml = empty($data['transfer']->jml) ? 0 : ($data['transfer']->jml); 

        $data['tunai'] = $this->M_Tu->get_tunai($tanggal_awal, $tanggal_akhir)->row();
        $data['tunai']->jml = empty($data['tunai']->jml) ? 0 : ($data['tunai']->jml);

        $data['total_transaksi'] = $this->M_Tu->get_total_transaksi($tanggal_awal, $tanggal_akhir)->row();
        $data['total_transaksi']->jml = empty($data['total_transaksi']->jml) ? 0 : ($data['total_transaksi']->jml);

        $data['all'] = $this->M_Tu->get_sum_all_pembayaran($tanggal_awal, $tanggal_akhir)->row();
        $data['all']->jml = empty($data['all']->jml) ? 0 : ($data['all']->jml);

        // Data bayarnya
        $data['pembayaran'] = $this->M_Tu->get_all_pembayaran($tanggal_awal, $tanggal_akhir)->result();

        // Tanggal
        $data['tanggal_awal'] = $tanggal_awal;
        $data['tanggal_akhir'] = $tanggal_akhir;

        $this->load->view("tu/summary_harian", $data);
    }

    public function rekap(){
        $data['kelas'] = $this->M_Tu->nav_kelas();
        $data['title'] = "Administrasi | Rekap";

        $data['active'] = "rekap";
        $data['menu_open'] = "nothing";
        $data['javascript'] = base_url().'assets/js/tu_rekap.js';

        $data['konten'] = 'tu/landingpage_rekap';
        $this->load->view("tu/header_footer", $data);
    }

    public function load_rekap($tanggal){
        $data_tanggal = explode("%20-%20" , $tanggal);
        
        $tanggal_awal = explode("-" , $data_tanggal[0]);
        $tanggal_awal = $tanggal_awal[2]."-".$tanggal_awal[0]."-".$tanggal_awal[1];
        
        $tanggal_akhir = explode("-" , $data_tanggal[1]);
        $tanggal_akhir = $tanggal_akhir[2]."-".$tanggal_akhir[0]."-".$tanggal_akhir[1];

        // Data Summary kotak kotak
        $data['transfer'] = $this->M_Tu->get_transfer($tanggal_awal, $tanggal_akhir)->row();
        $data['transfer']->jml = empty($data['transfer']->jml) ? 0 : ($data['transfer']->jml); 

        $data['tunai'] = $this->M_Tu->get_tunai($tanggal_awal, $tanggal_akhir)->row();
        $data['tunai']->jml = empty($data['tunai']->jml) ? 0 : ($data['tunai']->jml);

        $data['total_transaksi'] = $this->M_Tu->get_total_transaksi($tanggal_awal, $tanggal_akhir)->row();
        $data['total_transaksi']->jml = empty($data['total_transaksi']->jml) ? 0 : ($data['total_transaksi']->jml);

        $data['all'] = $this->M_Tu->get_sum_all_pembayaran($tanggal_awal, $tanggal_akhir)->row();
        $data['all']->jml = empty($data['all']->jml) ? 0 : ($data['all']->jml);

        // Data bayarnya
        $data['pembayaran'] = $this->M_Tu->get_all_pembayaran($tanggal_awal, $tanggal_akhir)->result();

        // Tanggal
        $data['tanggal_awal'] = $tanggal_awal;
        $data['tanggal_akhir'] = $tanggal_akhir;

        $this->load->view("tu/load_rekap", $data);
    }

    public function tagihan_psb(){
        $data['kelas'] = $this->M_Tu->nav_kelas();
        $data['tingkatan'] = $this->M_Tu->get_tingkatan()->result();
        $data['tagihan'] = $this->M_Tu->get_tagihan()->result();

        $data['title'] = "Administrasi | Tagihan";

        $data['javascript'] = base_url()."assets/js/tu_psb.js";
        $data['active'] = "tagihan_psb";
        $data['menu_open'] = "";

        $data['konten'] = 'tu/tagihan_psb';
        $this->load->view('tu/header_footer', $data);
    }


    //pembayaran function untuk tampil
    public function pembayaran(){
        $data['kelas'] = $this->M_Tu->nav_kelas();
        $data['siswa'] = $this->M_Tu->getSiswa()->result();

        $data['title'] = "Administrasi | Pembayaran";

        $data['javascript'] = base_url()."assets/js/tu_psb.js";
        $data['active'] = "pembayaran";
        $data['menu_open'] = "";

        $data['konten'] = "tu/view_pembayaran";
        $this->load->view('tu/header_footer', $data);
    }

    // Page Konfirmasi pembayaran
    public function konfirmasi_pembayaran(){
        $data['kelas'] = $this->M_Tu->nav_kelas();
        $data['pembayaran'] = $this->M_Tu->getPembayaran()->result();

        $data['title'] = "Administrasi | Konfirmasi Pembayaran";

        $data['javascript'] = base_url()."assets/js/tu_psb.js";
        $data['active'] = "konfirmasi_pembayaran";
        $data['menu_open'] = "";

        $data['konten'] = "tu/konfirmasi_pembayaran";
        $this->load->view('tu/header_footer', $data);
    }

    public function detail_konfirmasi($id){
        $data['kelas'] = $this->M_Tu->nav_kelas();
        $data['detail'] = $this->M_Tu->getDetailPembayaran($id)->row();

        $data['title'] = "Administrasi | Konfirmasi Pembayaran";

        $data['javascript'] = base_url()."assets/js/tu_psb.js";
        $data['active'] = "konfirmasi_pembayaran";
        $data['menu_open'] = "";

        $data['konten'] = "tu/detail_konfirmasi";
        $this->load->view('tu/header_footer', $data);
    }

    public function validasi_bayar(){
        $this->form_validation->set_rules('nis', 'NIS', 'required',
        array('required'=>'*Tolong Pilih Kolom NIS'));

        $this->form_validation->set_rules('bayar', 'Total Pembayaran', 'required|numeric|min_length[4]',
        array('required'=>'*Tolong Isi Kolom Total Pembayaran Dengan Benar'));
        
        if($this->form_validation->run() == FALSE){
            $this->pembayaran();
        }else{
            $this->add_pembayaran();
        }
    }
    
    public function validasi_tagihan(){
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div style="color:red; margin-bottom: 5px">', '</div>');
        
            $this->form_validation->set_rules('id_tingkatan', 'Angkatan', 'required',
            array('required'=>'*Mohon Di isi!'));
            $this->form_validation->set_rules('x_dsp', 'Biaya DSP', 'required|numeric');
            $this->form_validation->set_rules('x_seragam', 'Biaya Seragam', 'required|numeric');
            $this->form_validation->set_rules('x_spp', 'Biaya SPP Kelas 10', 'required|numeric');
            $this->form_validation->set_rules('xi_spp', 'Biaya SPP Kelas 11', 'required|numeric');
            $this->form_validation->set_rules('xii_spp', 'Biaya SPP Kelas 12', 'required|numeric');
            $this->form_validation->set_rules('xii_kunjin', 'Biaya Kunjin', 'required|numeric');

        if($this->form_validation->run() == FALSE){
            $this->view_addtagihan();
        }else{
            $this->add_tagihan();
        }
    }

    public function validasi_updatetagihan(){
        $this->load->library('form_validation');

        // jika edit
        $id_edit = $this->input->post('id');
        $this->form_validation->set_error_delimiters('<div style="color:red; margin-bottom: 5px">', '</div>');
        
            $this->form_validation->set_rules('id_tingkatan', 'Angkatan', 'required',
            array('required'=>'*Mohon Di isi!'));
            $this->form_validation->set_rules('x_dsp', 'Biaya DSP', 'required|numeric');
            $this->form_validation->set_rules('x_seragam', 'Biaya Seragam', 'required|numeric');
            $this->form_validation->set_rules('x_spp', 'Biaya SPP Kelas 10', 'required|numeric');
            $this->form_validation->set_rules('xi_spp', 'Biaya SPP Kelas 11', 'required|numeric');
            $this->form_validation->set_rules('xii_spp', 'Biaya SPP Kelas 12', 'required|numeric');
            $this->form_validation->set_rules('xii_kunjin', 'Biaya Kunjin', 'required|numeric');

        if($this->form_validation->run() == FALSE){
            $this->view_edittagihan($id);
        }else{
            $id = $this->input->post('id_tagihan');
            $id_tingkatan = $this->input->post('id_tingkatan');
            $x_dsp = $this->input->post('x_dsp');
            $x_seragam = $this->input->post('x_seragam');
            $x_spp = $this->input->post('x_spp');
            $xi_spp = $this->input->post('xi_spp');
            $xii_spp = $this->input->post('xii_spp');
            $xii_kunjin = $this->input->post('xii_kunjin');
            $status = $this->input->post('status');

            $data = array(
                'id_tingkatan' => $id_tingkatan,
                'x_dsp' => $x_dsp,
                'x_seragam' => $x_seragam,
                'x_spp' => $x_spp,
                'xi_spp' => $xi_spp,
                'xii_spp' => $xii_spp,
                'xii_kunjin' => $xii_kunjin,
                'status' => $status
            );

            $where = array(
                'id_tagihan' => $id
            );
        
            if($this->M_Tu->edit_tagihan($where,$data,'tagihan')){
                $this->session->set_flashdata('flash', 'Diedit !');
                redirect(base_url("index.php/Tatausaha/tagihan_psb"));
            }elseif($id_edit){
                $this->session->set_flashdata('flash', 'gagal');
                redirect(base_url("index.php/Tatausaha/view_edittagihan/'.$id_edit.'"));
            }else{
                $this->session->set_flashdata('flash', 'Diedit !');
                redirect(base_url("index.php/Tatausaha/tagihan_psb"));
            }    
        }
    }


    // untuk masuk ke view tambah tagihan
    public function view_addtagihan(){
        $data['kelas'] = $this->M_Tu->nav_kelas();
        $data['tingkatan'] = $this->M_Tu->get_tingkatan()->result();
        $data['tagihan'] = $this->M_Tu->get_tagihan()->result();

        $data['title'] = "Administrasi | Tagihan";

        $data['javascript'] = base_url()."assets/js/tu_psb.js";
        $data['active'] = "tagihan_psb";
        $data['menu_open'] = "";

        $data['konten'] = 'tu/tambah_tagihan';
        $this->load->view('tu/header_footer', $data);
    }

    public function view_edittagihan($id){
        $where = array('id' => $id);
        $data['kelas'] = $this->M_Tu->nav_kelas();
        $data['tingkatan'] = $this->M_Tu->get_tingkatan()->result();
        $data['tagihan'] = $this->M_Tu->getDataEditTagihan($id)->row();
        
        $data['title'] = "Administrasi | Tagihan";

        $data['javascript'] = base_url()."assets/js/tu_psb.js";
        $data['active'] = "tagihan_psb";
        $data['menu_open'] = "";

        $data['konten'] = 'tu/update_tagihan';
        $this->load->view('tu/header_footer', $data);
    }

    //ngarahin ke view edit tagihan
    function edit($id){
        $where = array('id' => $id);
        // $data['user'] = $this->M_Tu->edit_tagihan($where)->result();
        // $this->load->view('update_tagihan',$data);
    }
    

    public function add_tagihan(){
            
            if($this->M_Tu->insert_tagihan()){
                $this->session->set_flashdata('flash', 'berhasil');
                redirect(base_url("index.php/Tatausaha/tagihan_psb"));
            }else{
                $this->session->set_flashdata('flash', 'Gagal');
                redirect(base_url("index.php/Tatausaha/tagihan_psb"));
            }
    }

    //edit data tagihan
    public function edit_tagihan($id){
        $tingkatan = $this->input->post('id_tingkatan');
        $dsp = $this->input->post('x_dsp');
        $seragam = $this->input->post('x_seragam');
        $spp10 = $this->input->post('x_spp');
        $spp11 = $this->input->post('xi_spp');
        $spp12 = $this->input->post('xii_spp');
        $kunjin = $this->input->post('xii_kunjin');

        $data = array(
            'tingkatan' => $tingkatan,
            'x_dsp' => $dsp,
            'x_seragam' => $seragam,
            'x_spp' => $spp10,
            'xi_spp' => $spp11,
            'xii_spp' => $spp12,
            'xii_kunjin' => $kunjin

        );

        $where = array(
            'id_tagihan' => $id
        );
       
        if($this->M_Tu->edit_tagihan($where,$data,'tagihan')){
                $this->session->set_flashdata('flash', 'Diedit !');
                redirect(base_url("index.php/Tatausaha/tagihan_psb"));
            }else{
                $this->session->set_flashdata('flash', 'gagal');
                redirect(base_url("index.php/Tatausaha/tagihan_psb"));
            }
        }

    //delete tagihan
    public function delete_tagihan($idt){
       
        if($this->M_Tu->delete_mtagihan($idt)){
            $this->session->set_flashdata('flash', 'dihapus !');
            redirect(base_url("index.php/Tatausaha/tagihan_psb"));
        }else{
            $this->session->set_flashdata('flash', 'gagal');
            redirect(base_url("index.php/Tatausaha/tagihan_psb"));
        }
    }


    public function load_info_tagihan_dsp($id_kelas, $id_tingkat){
        $data = $this->M_Tu->info_tagihan_dsp($id_kelas, $id_tingkat)->result();
        echo json_encode($data);
    }

    public function load_detail_tagihan($nis){
        $data = $this->M_Tu->load_detail($nis)->result();
        
        foreach($data as $a){?>
            <tr>
                <td>Rp. <?= number_format($a->dsp_x); ?></td>
                <td>Rp. <?= number_format($a->seragam_x); ?></td>
                <td>Rp. <?= number_format($a->spp_x); ?></td>
                <td>Rp. <?= number_format($a->spp_xi); ?></td>
                <td>Rp. <?= number_format($a->spp_xii); ?></td>
                <td>Rp. <?= number_format($a->total); ?></td>
            </tr>
        <?php
        }
        if(empty($data)){
            echo "<tr><td colspan='5'>Tidak Ada Data !</td></tr>";
        }
    }

    public function get_riwayatBayar($nis){
        $data = $this->M_Tu->getRiwayat($nis)->result();

        foreach($data as $a){?>
            <tr>
                <td><?= $a->nis;?></td>
                <td><?= $a->nama_siswa;?></td>
                <td><?= $a->timestamp;?></td>
                <td><?= $a->total_pembayaran;?></td>
                <td><?= $a->metode_pembayaran;?></td>
                <td><?= $a->status;?></td>
                <td><?= $a->nama_petugas;?></td>
            </tr>
        <?php
        }
        if(empty($data)){
            echo "<tr><td colspan='7'>Tidak Ada Data !</td></tr>";
        }
    }

    public function load_input_tagihan($nis){
        $data = $this->M_Tu->getTagihanSiswa($nis)->row();

        ?>
         <table class="table text-center"> 
            <tr>
                <th> Seragam </th>
                <th> DSP </th>
                <th> SPP tahun ke 1 </th>
                <th> SPP tahun ke 2 </th>
                <th> SPP tahun ke 3 </th>
                <th> Total </th>
            </tr>
            <div class="form-group">
            <tr>
                <th><input type="text" class="form-control" readonly name="x_seragam" value="<?= $data->seragam_x; ?>"></th>
                <th><input type="text" class="form-control" readonly name="x_dsp" value="<?= $data->dsp_x; ?>"></th>
                <th><input type="text" class="form-control" readonly name="x_spp" value="<?= $data->spp_x; ?>"></th>
                <th><input type="text" class="form-control" readonly name="xi_spp" value="<?= $data->spp_xi; ?>"></th>
                <th><input type="text" class="form-control" readonly name="xii_spp" value="<?= $data->spp_xii; ?>"></th>
                <th><input type="text" class="form-control" readonly name="total" value="<?= $data->total; ?>"><br></th>
                <th><input type="hidden" class="form-control" name="email" value="<?= $data->email; ?>"></th>
                <th><input type="hidden" name="nama" value="<?= $data->nama_siswa; ?>"></th>
            </tr>
            </div>
        </table>
        <?php
        
    }

    //add pembayaran
    public function add_pembayaran(){
        $total_pembayaran = $this->input->post('bayar');
        $metode_pembayaran = $this->input->post('metode');
        $nis = $this->input->post('nis');
        $x_seragam = $this->input->post('x_seragam');
        $x_dsp = $this->input->post('x_dsp');
        $x_spp = $this->input->post('x_spp');
        $xi_spp = $this->input->post('xi_spp');
        $xii_spp = $this->input->post('xii_spp');
        $email = $this->input->post('email');
        $nama = $this->input->post('nama');
        $nama_petugas = $this->session->userdata('nama_petugas');

        if ($this->M_Tu->insert_pembayaran($total_pembayaran, $metode_pembayaran, $nis, $nama_petugas)){
            $this->M_Tu->update_tagihanSiswa($x_seragam, $x_dsp, $x_spp, $xi_spp, $xii_spp, $total_pembayaran, $nis);
            $ci = get_instance();
            $ci->load->library('email');
            $config['protocol'] = "smtp";
            $config['smtp_host'] = "ssl://smtp.gmail.com";
            $config['smtp_port'] = "465";
            $config['smtp_user'] = "mrgreed1990@gmail.com";
            $config['smtp_pass'] = "greedy_1990";
            $config['charset'] = "utf-8";
            $config['mailtype'] = "html";
            $config['newline'] = "\r\n";
            $ci->email->initialize($config);
            $ci->email->from('smkn11bdg@gmail.com', 'TU SMKN 11 Bandung');
            $ci->email->to($email);
            $ci->email->subject('Pembayaran Sukses!');
            $ci->email->message("
                Telah Melakukan Pembayaran Dengan Keterangan Berikut:
                <ul>
                    <li>Atas Nama : <b>".$nama."<b></li>
                    <li>Total Pembayaran : <b>Rp.".$total_pembayaran."<b></li>
                    <li>Metode Pemabayaran : <b>".$metode_pembayaran."<b></li>
                    <li>Dikonfirmasi Oleh : <b>".$this->session->userdata('nama_petugas')."<b></li>
                    <li>Status : <b>'Dikonfirmasi'<b></li>
                </ul>
            ");
            if($this->email->send()){
                $this->session->set_flashdata('flash', 'berhasil !');
                redirect(base_url("index.php/Tatausaha/pembayaran"));
            }else{
                $this->session->set_flashdata('flash', 'gagal email !');
                redirect(base_url("index.php/Tatausaha/pembayaran"));
            }
        }else{
            $this->session->set_flashdata('flash', 'Gagal');
            redirect(base_url("index.php/Tatausaha/pembayaran"));
        }
    }

    public function update_konfirmasi($id_pembayaran){
        $nama_petugas = $this->session->userdata('nama_petugas');
        $bayar = $this->M_Tu->getBayarById($id_pembayaran)->row();
        if($this->M_Tu->updateKonfirmasi($id_pembayaran, $nama_petugas)){
            $this->M_Tu->update_tagihanSiswa($bayar->seragam_x, $bayar->dsp_x, $bayar->spp_x, $bayar->spp_xi, $bayar->spp_xii, $bayar->total_pembayaran, $bayar->nis);
            $ci = get_instance();
            $ci->load->library('email');
            $config['protocol'] = "smtp";
            $config['smtp_host'] = "ssl://smtp.gmail.com";
            $config['smtp_port'] = "465";
            $config['smtp_user'] = "mrgreed1990@gmail.com";
            $config['smtp_pass'] = "greedy_1990";
            $config['charset'] = "utf-8";
            $config['mailtype'] = "html";
            $config['newline'] = "\r\n";
            $ci->email->initialize($config);
            $ci->email->from('smkn11bdg@gmail.com', 'TU SMKN 11 Bandung');
            $ci->email->to($bayar->email);
            $ci->email->subject('Pembayaran Sukses!');
            $ci->email->message("
                Telah Melakukan Pembayaran Dengan Keterangan Berikut:
                <ul>
                    <li>Atas Nama : <b>".$bayar->nama."<b></li>
                    <li>Total Pembayaran : <b>Rp.".$bayar->total_pembayaran."<b></li>
                    <li>Metode Pemabayaran : <b>".$bayar->metode_pembayaran."<b></li>
                    <li>Dikonfirmasi Oleh : <b>".$this->session->userdata('nama_petugas')."<b></li>
                    <li>Status : <b>'Dikonfirmasi'<b></li>
                </ul>
            ");
            if($this->email->send()){
                $this->session->set_flashdata('flash','Diedit !');
                redirect(base_url('index.php/Tatausaha/konfirmasi_pembayaran'));
            }else{
                $this->session->set_flashdata('flash', 'gagal email !');
                redirect(base_url('index.php/Tatausaha/konfirmasi_pembayaran'));
            }
        }else{
            $this->session->set_flashdata('flash','Gagal !');
            redirect(base_url('index.php/Tatausaha/konfirmasi_pembayaran'));
        }
    }

    public function edit_konfirmasi($id){
        $total = $this->input->post("total");
        $where = $this->input->post("idp");

        if($this->M_Tu->editKonfirmasi($where,$total)){
            $this->session->set_flashdata('flash', 'edited');
            redirect(base_url("index.php/Tatausaha/konfirmasi_pembayaran"));
        }else{
            $this->session->set_flashdata('flash', 'failed');
            redirect(base_url("index.php/Tatausaha/konfirmasi_pembayaran"));
        }
    }

}
?>